<?php

namespace App\Http\Controllers;

use App\Console\Commands\Player;
use App\Models\Settings;
use App\Models\Ticket;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SiteController extends Controller
{
    public function getReferals(Request $request)
    {
        $user = Auth::guard()->user();
        if (!empty($user)) {
            $referal_users = User::where('referer_id', $user->id)->get();
            $referals = [];
            $count = 1;
            foreach ($referal_users as $referal) {
                $referals[] = [
                    'counter' => $count,
                    'email' => $referal->email,
                    'balance' => $referal->balance,
                    'lotteries' => $referal->lotteries,
                    'created_at' => $referal->created_at->format('Y-m-d\ H:i:s'),
                    'last_login' => $referal->last_login->format('Y-m-d\ H:i:s'),
                ];
                $count++;
            }
            $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
            return response()->json(['success' => true, 'referals' => $referals, 'ref_link' => $url . '?r=' . $user->ref_link]);
        }
        return response()->json(['success' => false]);
    }

    public function runPlayer()
    {
        $player_stop = Settings::where('name', 'player_stop')->first();
        $player_stop->value = 0;
        $player_stop->save();

        if ($player_stop !== 1) {
            $hour = (int)date('H', (new DateTime('now'))->getTimestamp());
            $time_now = time();
            $from_date = (new DateTime('now 00:00:00'))->getTimestamp();
            $minutes_remain = 1440 - round(($time_now - $from_date) / 60, 0);
            $all_users = User::whereNotIn('tickets_remain', [0])->count('id');
            if (round($all_users / $minutes_remain, 0) > 1) {
                $users_to_play = $hour + random_int(1, round($all_users / $minutes_remain, 0));
            } else {
                $users_to_play = $hour;
            }

            $random_users = User::orderBy(DB::raw('RAND()'))->whereNotIn('tickets_remain', [0])->take($users_to_play)->get();
            foreach ($random_users as $random_user) {
                // Рандомный юзер берет рандомное кол-во билетов
                $tickets_to_take = random_int(1, $random_user->tickets_remain);
                for ($i = 1; $i <= $tickets_to_take; $i++) {
                    Ticket::createTicket($random_user);
                }
                $random_user->tickets_remain -= $tickets_to_take;
                $random_user->save();
            }
            $player = Settings::where('name', 'player')->first();
            $player->value = $time_now;
            $player->save();
        }
        return response()->json(['success' => true]);
    }

    public function stopPlayer()
    {
        $player_stop = Settings::where('name', 'player_stop')->first();
        $player_stop->value = 1;
        $player_stop->save();
        return response()->json(['success' => true]);
    }

    public function getPlayer()
    {
        $player = Settings::where('name', 'player')->first();
        $now = time();
        $status = false;
        if (round(($now - $player->value), 0) < 80) {
            $status = true;
        }
        return response()->json(['success' => true, 'player' => ['value' => date('Y-m-d H:i:s', $player->value), 'status' => $status]]);
    }

    public function runWinner()
    {
        $winner_stop = Settings::where('name', 'winner_stop')->first();
        $winner_stop->value = 0;
        $winner_stop->save();

        if ($winner_stop !== 1) {
            $time_now = time();
            $from_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->getTimestamp());
            $to_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->modify('+1 day')->getTimestamp());
            $date_start = (new DateTime('now 00:00:00'))->modify('+1 day -1 minute')->getTimestamp();
            if (round(($date_start - $time_now) / 60, 0) > 1) {
                $winner = Settings::where('name', 'winner')->first();
                $winner->value = $time_now;
                $winner->save();
                return response()->json(['success' => true]);
            }

            $real_participants = Ticket::whereBetween('created_at', [$from_date, $to_date])->distinct()->count('user_id') ?? 0;
            $adv_price = Settings::where('name', 'adv_price')->first()->value;
            $today_fond = $real_participants / 1000 * $adv_price;
            $first_place = Settings::where('name', 'first_place')->first()->value;
            $first_place_val = $today_fond * $first_place / 100 ?? 0;
            $second_place = Settings::where('name', 'second_place')->first()->value;
            $second_place_val = $today_fond * $second_place / 100 / 10 ?? 0;
            $third_place = Settings::where('name', 'third_place')->first()->value;
            $third_place_val = $today_fond * $third_place / 100 / 100 ?? 0;

            // Выбираем 1 рандомное 1 место из не реальных юзеров
            $fake_users = User::where('is_fake', 1)->get('id');
            $fake_users_ids = [];
            foreach ($fake_users as $fake_user) {
                $fake_users_ids[] = $fake_user->id;
            }
            $ticket = Ticket::whereIn('user_id', $fake_users_ids)->whereBetween('created_at', [$from_date, $to_date])->orderBy(DB::raw('RAND()'))->take(1)->first();
            $ticket->win = 1;
            $ticket->cash = $first_place_val;
            $ticket->level = 1;
            $ticket->save();
            // Выбираем 10 рандомных 2-х мест
            $random_tickets = Ticket::orderBy(DB::raw('RAND()'))->take(10)->get();
            foreach ($random_tickets as $random_ticket) {
                $random_ticket->win = 1;
                $random_ticket->cash = $second_place_val;
                $random_ticket->level = 2;
                $random_ticket->save();
            }
            // Выбираем 100 рандомных 3-х мест
            $random_tickets = Ticket::orderBy(DB::raw('RAND()'))->take(100)->get();
            foreach ($random_tickets as $random_ticket) {
                $random_ticket->win = 1;
                $random_ticket->cash = $third_place_val;
                $random_ticket->level = 3;
                $random_ticket->save();
            }
            // Чистим число посещений
            $all_visitors_day = Settings::where('name', 'all_visitors_day')->first();
            $all_visitors_day->value = 0;
            $all_visitors_day->save();
            $new_visitors_day = Settings::where('name', 'new_visitors_day')->first();
            $new_visitors_day->value = 0;
            $new_visitors_day->save();
            // Фонд суперлотереи
            $superlottery_fund = Settings::where('name', 'superlottery_fund')->first();
            $superlottery_fund->value += round($today_fond * 0.1,2);
            $superlottery_fund->save();

            $all_users = User::get();
            foreach ($all_users as $user) {
                $user->tickets_remain = $user->lotteries;
                $user->save();
            }

            $winner = Settings::where('name', 'winner')->first();
            $winner->value = $time_now;
            $winner->save();
        }
        return response()->json(['success' => true]);
    }

    public function stopWinner()
    {
        $winner_stop = Settings::where('name', 'winner_stop')->first();
        $winner_stop->value = 1;
        $winner_stop->save();
        return response()->json(['success' => true]);
    }

    public function getWinner()
    {
        $winner = Settings::where('name', 'winner')->first();
        $now = time();
        $status = false;
        if (round(($now - $winner->value), 0) < 80) {
            $status = true;
        }
        return response()->json(['success' => true, 'winner' => ['value' => date('Y-m-d H:i:s', $winner->value), 'status' => $status]]);
    }

    public function visitor(Request $request)
    {
        $is_visitor = $request->cookie('visitor');
        $all_visitors = Settings::where('name', 'all_visitors')->first();
        $all_visitors->value += 1;
        $all_visitors->save();
        $all_visitors_day = Settings::where('name', 'all_visitors_day')->first();
        $all_visitors_day->value += 1;
        $all_visitors_day->save();
        if (!$is_visitor) {
            $new_visitors = Settings::where('name', 'new_visitors')->first();
            $new_visitors->value += 1;
            $new_visitors->save();
            $new_visitors_day = Settings::where('name', 'new_visitors_day')->first();
            $new_visitors_day->value += 1;
            $new_visitors_day->save();
            $hours = date("H", time());
            $minutes = date("i", time());
            $last = 24*60 - $hours*60 - $minutes;

            return response()->json(['success' => true], 200)
                ->withCookie(cookie('visitor', true, $last));
        }
        return response()->json(['success' => true], 200);
    }

    public function getAdvs(Request $request)
    {
        $advs = [
            'leaderboard_bottom' => Settings::where('name', 'leaderboard_bottom')->first()->value,
            'leaderboard_center' => Settings::where('name', 'leaderboard_center')->first()->value,
            'leaderboard_top' => Settings::where('name', 'leaderboard_top')->first()->value,
            'rect_left' => Settings::where('name', 'rect_left')->first()->value,
            'rect_right' => Settings::where('name', 'rect_right')->first()->value,
            'skyscrap_left' => Settings::where('name', 'skyscrap_left')->first()->value,
            'skyscrap_right' => Settings::where('name', 'skyscrap_right')->first()->value,
        ];
        return response()->json(['success' => true, 'advs' => $advs], 200);
    }

    /**
     * Ее нужно дергать раз в минуту
     */
    public function getTodayStats(Request $request)
    {
        $from_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->getTimestamp());
        $to_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->modify('+1 day')->getTimestamp());
        $real_participants = Ticket::whereBetween('created_at', [$from_date, $to_date])->distinct()->count('user_id') ?? 0;
        $users = User::count('id');
        $tickets = Ticket::whereBetween('created_at', [$from_date, $to_date])->count('user_id');
        $adv_price = Settings::where('name', 'adv_price')->first()->value;
        $today_fond = $real_participants / 1000 * $adv_price - 0.1 * $real_participants / 1000 * $adv_price;

        $first_place = Settings::where('name', 'first_place')->first()->value;
        $first_place_val = $today_fond * $first_place / 100 ?? 0;
        $second_place = Settings::where('name', 'second_place')->first()->value;
        $second_place_val = $today_fond * $second_place / 100 / 10 ?? 0;
        $third_place = Settings::where('name', 'third_place')->first()->value;
        $third_place_val = $today_fond * $third_place / 100 / 100 ?? 0;

        $table = [
            [
                'place' => 1, 'users' => 1, 'perc' => $first_place . '%',
                'total' => round($first_place_val, 2) . '$',
                'chance' => $real_participants ? round(100 / $real_participants, 2) . '%' : 0
            ],
            [
                'place' => 2, 'users' => 10, 'perc' => $second_place . '%',
                'total' => round($second_place_val, 2) . '$',
                'chance' => $real_participants ? (round(1000 / $real_participants, 2) > 100 ? 100 : round(1000 / $real_participants, 2)) . '%' : 0
            ],
            [
                'place' => 3, 'users' => 100, 'perc' => $third_place . '%',
                'total' => round($third_place_val, 2) . '$',
                'chance' => $real_participants ? (round(10000 / $real_participants, 2) > 100 ? 100 : round(10000 / $real_participants, 2)) . '%' : 0
            ],
        ];
        $fields = [
            ['key' => 'place', 'label' => 'Место'],
            ['key' => 'users', 'label' => 'Победителей'],
            ['key' => 'perc', 'label' => 'Процент фонда'],
            ['key' => 'total', 'label' => 'Выигрыш'],
            ['key' => 'chance', 'label' => 'Шанс'],
        ];

        return response()->json([
            'success' => true,
            'stats' => [
                'all_users' => $users,
                'players' => $real_participants,
                'today_fond' => round($today_fond, 2) . '$',
                'tickets_today' => $tickets,
                'items' => $table,
                'fields' => $fields,
            ]
        ], 200);
//        Фонд суперлотереи
    }

    public function getYesturdayStats(Request $request)
    {
        $from_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->modify('-1 day')->getTimestamp());
        $to_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->getTimestamp());
        $real_participants = Ticket::whereBetween('created_at', [$from_date, $to_date])->distinct()->count('user_id');
        $real_winners = Ticket::where('win', 1)->whereBetween('created_at', [$from_date, $to_date])->distinct()->count('user_id');

        $tickets = Ticket::whereBetween('created_at', [$from_date, $to_date])->count('user_id');
        $adv_price = Settings::where('name', 'adv_price')->first()->value;
        $yesturday_fond = $real_participants / 1000 * $adv_price - 0.1 * $real_participants / 1000 * $adv_price;

        $first_place = Settings::where('name', 'first_place')->first()->value;
        $first_place_val = $yesturday_fond * $first_place / 100 ?? 0;
        $second_place = Settings::where('name', 'second_place')->first()->value;
        $second_place_val = $yesturday_fond * $second_place / 100 / 10 ?? 0;
        $third_place = Settings::where('name', 'third_place')->first()->value;
        $third_place_val = $yesturday_fond * $third_place / 100 / 100 ?? 0;

        $win_tickets = Ticket::where('win', 1)->get();
        $all_cash = 0;
        foreach ($win_tickets as $win_ticket) {
            $all_cash += $win_ticket->cash;
        }

        $table = [
            [
                'place' => 1, 'users' => 1, 'perc' => $first_place . '%',
                'total' => round($first_place_val, 2) . '$',
                'chance' => $real_participants ? round(100 / $real_participants, 2) . '%' : 0
            ],
            [
                'place' => 2, 'users' => 10, 'perc' => $second_place . '%',
                'total' => round($second_place_val, 2) . '$',
                'chance' => $real_participants ? (round(1000 / $real_participants, 2) > 100 ? 100 : round(1000 / $real_participants, 2)) . '%' : 0
            ],
            [
                'place' => 3, 'users' => 100, 'perc' => $third_place . '%',
                'total' => round($third_place_val, 2) . '$',
                'chance' => $real_participants ? (round(10000 / $real_participants, 2) > 100 ? 100 : round(10000 / $real_participants, 2)) . '%' : 0
            ],
        ];
        $fields = [
            ['key' => 'place', 'label' => 'Место'],
            ['key' => 'users', 'label' => 'Победителей'],
            ['key' => 'perc', 'label' => 'Процент фонда'],
            ['key' => 'total', 'label' => 'Выигрыш'],
            ['key' => 'chance', 'label' => 'Шанс'],
        ];

        return response()->json([
            'success' => true,
            'yesturday_stats' => [
                'all_users' => $real_participants,
                'yesturday_fond' => round($yesturday_fond, 2) . '$',
                'tickets_yesturday' => $tickets,
                'winners' => $real_winners,
                'first_place_val' => round($first_place_val, 2) . '$',
                'all_cash' => round($all_cash, 2) . '$',
                'items' => $table,
                'fields' => $fields,
            ]
        ], 200);
        //Размер фонда за вчера, участников, билетов
    }

    public function getAlltimeStats(Request $request)
    {
//        Всего пользователей
//        Всего выплачено
//        Всего победителей
//        Максимальный дневной джекпот
//        Максимальный джекпот суперлотереи
    }

    public function getYesturdayWins(Request $request)
    {
//        Блок выигрышные номера за: дата (вчера)
//        1 место
//        2 место
//        3 место
//        таблица список номеров и выигрыш
    }

    public function checkLottery(Request $request)
    {
//        проверить номер - если выигрышный, переводить на баланс юзера
    }

}
