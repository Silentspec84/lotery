(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./frontend/views/settings/index.vue":
/*!*******************************************!*\
  !*** ./frontend/views/settings/index.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=72af40e6&scoped=true& */ "./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./frontend/views/settings/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& */ "./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "72af40e6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/views/settings/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/views/settings/index.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./frontend/views/settings/index.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&":
/*!****************************************************************************************************!*\
  !*** ./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_dist_cjs_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_72af40e6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true&":
/*!**************************************************************************************!*\
  !*** ./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=72af40e6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_72af40e6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/settings/index.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Advs_Leaderboardtop__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/Advs/Leaderboardtop */ "./frontend/components/Advs/Leaderboardtop/index.vue");
/* harmony import */ var _components_Advs_Skyscrapleft__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/Advs/Skyscrapleft */ "./frontend/components/Advs/Skyscrapleft/index.vue");
/* harmony import */ var _components_Advs_Skyscrapright__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/Advs/Skyscrapright */ "./frontend/components/Advs/Skyscrapright/index.vue");
/* harmony import */ var _components_Advs_Rectleft__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/components/Advs/Rectleft */ "./frontend/components/Advs/Rectleft/index.vue");
/* harmony import */ var _components_Advs_Rectright__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/components/Advs/Rectright */ "./frontend/components/Advs/Rectright/index.vue");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Settings",
  components: {
    Leaderboardtop: _components_Advs_Leaderboardtop__WEBPACK_IMPORTED_MODULE_2__["default"],
    Skyscrapleft: _components_Advs_Skyscrapleft__WEBPACK_IMPORTED_MODULE_3__["default"],
    Skyscrapright: _components_Advs_Skyscrapright__WEBPACK_IMPORTED_MODULE_4__["default"],
    Rectleft: _components_Advs_Rectleft__WEBPACK_IMPORTED_MODULE_5__["default"],
    Rectright: _components_Advs_Rectright__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      pay: {
        acc: null,
        val: null
      },
      items: [{
        text: 'Главная',
        to: '/'
      }, {
        text: 'Настройки',
        active: true
      }],
      new_email: '',
      check_password: '',
      new_password: '',
      referals: [],
      ref_link: '',
      fields: [{
        key: 'counter',
        label: '№',
        sortable: true
      }, {
        key: 'email',
        label: 'Email',
        sortable: true
      }, {
        key: 'balance',
        label: 'Баланс',
        sortable: true
      }, {
        key: 'lotteries',
        label: 'Лотерей в день',
        sortable: true
      }, {
        key: 'created_at',
        label: 'Присоединился',
        sortable: true
      }, {
        key: 'last_login',
        label: 'Последний вход',
        sortable: true
      }],
      pay_settings: {}
    };
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('user', ['updateUser'])), {}, {
    copyToClipboard: function copyToClipboard($event) {
      var text = $event.target.textContent.split('(')[0].replace(/^\s$/g, '');
      var textArea = document.createElement('textarea');
      textArea.value = text; //предотвращаем скроллинг страницы

      textArea.style.top = '0';
      textArea.style.left = '0';
      textArea.style.position = 'fixed';
      document.body.appendChild(textArea);
      textArea.focus();
      textArea.select();

      try {
        document.execCommand('copy');
        console.log('ok'); // this.$toastSuccess('Скопировано');
      } catch (err) {// this.$toastError('Произошла ошибка. Повторите попытку позже.');
      }

      document.body.removeChild(textArea);
    },
    getReferals: function getReferals() {
      var _this = this;

      axios.post('/api/get-referals').then(function (response) {
        if (!response.data.success) {} else {
          _this.referals = response.data.referals;
          _this.ref_link = response.data.ref_link;
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    changeEmail: function changeEmail() {
      axios.post('/api/change-email', {
        email: this.new_email,
        password: this.check_password
      }).then(function (response) {
        if (!response.data.success) {} else {}
      })["catch"](function (err) {
        console.log(err);
      });
    },
    changePassword: function changePassword() {
      axios.post('/api/change-password', {
        password: this.check_password,
        new_password: this.new_password
      }).then(function (response) {
        if (!response.data.success) {} else {}
      })["catch"](function (err) {
        console.log(err);
      });
    },
    getPayData: function getPayData() {
      var _this2 = this;

      axios.post('/api/get-pay-data').then(function (response) {
        if (!response.data.success) {} else {
          _this2.pay_settings = response.data.pay_settings;
          console.log(response.data);
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    withdraw: function withdraw(pay_system) {
      console.log(pay_system.account_reg_exp);
      console.log(this.pay.acc.match(pay_system.account_reg_exp));

      if (!this.pay.acc.match(pay_system.account_reg_exp)) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
          icon: 'error',
          title: 'Ошибка',
          text: 'Неверный формат номера счета!'
        });
        return;
      }

      if (this.pay.val < 5) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
          icon: 'error',
          title: 'Ошибка',
          text: 'Минимальный вывод - 5$!'
        });
        return;
      }

      axios.post('/api/withdraw', {
        pay_system: pay_system,
        pay_data: this.pay
      }).then(function (response) {
        if (!response.data.success) {} else {
          return true;
        }
      })["catch"](function (err) {
        console.log(err);
      });
    }
  }),
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['user'])),
  mounted: function mounted() {
    this.getReferals();
    this.getPayData();
  },
  metaInfo: {
    title: 'Бесплатная лотерея: настройки',
    meta: [{
      charset: 'utf=8'
    }, {
      vmid: 'description',
      property: 'description',
      content: 'Vue App'
    }, {
      vmid: 'title',
      property: 'title',
      content: 'Vue App'
    }, {
      vmid: 'og:title',
      property: 'og:title',
      content: 'Vue App'
    }, {
      vmid: 'og:description',
      property: 'og:description',
      content: 'Vue App'
    }],
    htmlAttrs: {
      lang: 'ru'
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.container[data-v-72af40e6] {\n    padding: 0;\n}\n.col-l[data-v-72af40e6] {\n    padding-left: 1%\n}\n.col-r[data-v-72af40e6] {\n    padding-right: 1%\n}\n", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader/dist/cjs.js??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=style&index=0&id=72af40e6&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/settings/index.vue?vue&type=template&id=72af40e6&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "b-container",
    { staticClass: "h-100 mb-4 mt-6", attrs: { fluid: "" } },
    [
      _c(
        "b-row",
        [
          _c(
            "b-col",
            { staticClass: "mt-4 col-l", attrs: { cols: "2" } },
            [_c("Skyscrapleft"), _vm._v(" "), _c("Rectleft")],
            1
          ),
          _vm._v(" "),
          _c(
            "b-col",
            { staticClass: "mt-4", attrs: { cols: "8" } },
            [
              _c(
                "b-container",
                { attrs: { fluid: "" } },
                [
                  _c("b-breadcrumb", { attrs: { items: _vm.items } }),
                  _vm._v(" "),
                  _c("b-card", { attrs: { "no-body": "" } }, [
                    _c(
                      "h1",
                      { staticClass: "justify-content-center text-center p-4" },
                      [_vm._v("Настройки")]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "b-card",
                    {
                      staticClass: "mt-4",
                      attrs: { "no-body": "", "bg-variant": "light" }
                    },
                    [
                      _c(
                        "b-tabs",
                        { attrs: { card: "" } },
                        [
                          _c(
                            "b-tab",
                            { attrs: { title: "Основные", active: "" } },
                            [
                              _c(
                                "b-card",
                                {
                                  attrs: {
                                    title: "Изменить Email",
                                    "sub-title":
                                      "Вы можете изменить Email. Текущий email: " +
                                      _vm.user.email
                                  }
                                },
                                [
                                  _c(
                                    "b-card-text",
                                    [
                                      _c(
                                        "b-form",
                                        [
                                          _c(
                                            "b-form-group",
                                            {
                                              attrs: {
                                                label: "Новый Email адрес:",
                                                "label-for": "input-1"
                                              }
                                            },
                                            [
                                              _c("b-form-input", {
                                                attrs: {
                                                  id: "input-1",
                                                  type: "email",
                                                  placeholder:
                                                    "Новый Email адрес",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.new_email,
                                                  callback: function($$v) {
                                                    _vm.new_email = $$v
                                                  },
                                                  expression: "new_email"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "b-form-group",
                                            {
                                              attrs: {
                                                label: "Введите пароль:",
                                                "label-for": "input-2"
                                              }
                                            },
                                            [
                                              _c("b-form-input", {
                                                attrs: {
                                                  id: "input-2",
                                                  type: "password",
                                                  placeholder: "Введите пароль",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.check_password,
                                                  callback: function($$v) {
                                                    _vm.check_password = $$v
                                                  },
                                                  expression: "check_password"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "b-button",
                                    {
                                      attrs: { variant: "outline-primary" },
                                      on: { click: _vm.changeEmail }
                                    },
                                    [_vm._v("Сохранить")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "b-card",
                                {
                                  staticClass: "mt-8",
                                  attrs: {
                                    title: "Смена пароля",
                                    "sub-title":
                                      "Для смены пароля введите старый пароль и новый"
                                  }
                                },
                                [
                                  _c(
                                    "b-card-text",
                                    [
                                      _c(
                                        "b-form",
                                        [
                                          _c(
                                            "b-form-group",
                                            {
                                              attrs: {
                                                label: "Старый пароль:",
                                                "label-for": "input-1"
                                              }
                                            },
                                            [
                                              _c("b-form-input", {
                                                attrs: {
                                                  id: "input-1",
                                                  type: "password",
                                                  placeholder:
                                                    "Введите старый пароль",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.check_password,
                                                  callback: function($$v) {
                                                    _vm.check_password = $$v
                                                  },
                                                  expression: "check_password"
                                                }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "b-form-group",
                                            {
                                              attrs: {
                                                label: "Новый пароль:",
                                                "label-for": "input-2"
                                              }
                                            },
                                            [
                                              _c("b-form-input", {
                                                attrs: {
                                                  id: "input-2",
                                                  type: "password",
                                                  placeholder:
                                                    "Введите новый пароль",
                                                  required: ""
                                                },
                                                model: {
                                                  value: _vm.new_password,
                                                  callback: function($$v) {
                                                    _vm.new_password = $$v
                                                  },
                                                  expression: "new_password"
                                                }
                                              })
                                            ],
                                            1
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "b-button",
                                    { attrs: { variant: "outline-primary" } },
                                    [_vm._v("Сохранить")]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "b-tab",
                            { attrs: { title: "Вывод средств" } },
                            [
                              _c(
                                "b-card-text",
                                [
                                  _c("h3", [
                                    _vm._v("Баланс составляет: "),
                                    _c(
                                      "span",
                                      {
                                        staticStyle: {
                                          "font-size": "large",
                                          "font-weight": "bold",
                                          color: "green"
                                        }
                                      },
                                      [
                                        _vm._v(
                                          _vm._s(_vm.user.balance.toFixed(2)) +
                                            "$"
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm.user.balance > 4
                                    ? _c(
                                        "b-tabs",
                                        { attrs: { card: "" } },
                                        _vm._l(_vm.pay_settings, function(
                                          pay_system
                                        ) {
                                          return _c(
                                            "b-tab",
                                            {
                                              key: pay_system.id,
                                              attrs: {
                                                title: pay_system.name,
                                                active: ""
                                              }
                                            },
                                            [
                                              _c(
                                                "b-card-text",
                                                [
                                                  _c(
                                                    "b-form",
                                                    [
                                                      _c(
                                                        "b-form-group",
                                                        {
                                                          attrs: {
                                                            id: "input-group-1",
                                                            label:
                                                              "Номер счета:",
                                                            "label-for":
                                                              "input-1"
                                                          }
                                                        },
                                                        [
                                                          _c("b-form-input", {
                                                            attrs: {
                                                              id: "input-1",
                                                              placeholder:
                                                                pay_system.account_ex,
                                                              required: ""
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.pay.acc,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.pay,
                                                                  "acc",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "pay.acc"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "b-form-group",
                                                        {
                                                          attrs: {
                                                            id: "input-group-2",
                                                            label: "Сумма:",
                                                            "label-for":
                                                              "input-2",
                                                            description:
                                                              "С учетом комиссии платежной системы " +
                                                              pay_system.commission +
                                                              "%: " +
                                                              (
                                                                _vm.pay.val -
                                                                (_vm.pay.val *
                                                                  pay_system.commission) /
                                                                  100
                                                              ).toFixed(2)
                                                          }
                                                        },
                                                        [
                                                          _c("b-form-input", {
                                                            attrs: {
                                                              id: "input-2",
                                                              placeholder:
                                                                "Сумма",
                                                              required: ""
                                                            },
                                                            model: {
                                                              value:
                                                                _vm.pay.val,
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.pay,
                                                                  "val",
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "pay.val"
                                                            }
                                                          })
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "b-button",
                                                        {
                                                          attrs: {
                                                            variant: "primary",
                                                            block: ""
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.withdraw(
                                                                pay_system
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Вывести")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        }),
                                        1
                                      )
                                    : _vm._e(),
                                  _vm._v(" "),
                                  _vm.user.balance < 5
                                    ? _c("h4", [
                                        _vm._v(
                                          "Вывод средств доступен только от 5$. На твоем депозите пока меньше 5$."
                                        )
                                      ])
                                    : _vm._e()
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "b-tab",
                            { attrs: { title: "Мои рефералы" } },
                            [
                              _c(
                                "b-card-text",
                                [
                                  _c("h4", [_vm._v("Реферальная ссылка:")]),
                                  _vm._v(" "),
                                  _c(
                                    "b",
                                    { on: { click: _vm.copyToClipboard } },
                                    [_vm._v(_vm._s(_vm.ref_link))]
                                  ),
                                  _vm._v(" "),
                                  _c("i", {
                                    staticClass: "mdi mdi-content-copy"
                                  }),
                                  _vm._v(" "),
                                  _c("h4", { staticClass: "mt-12" }, [
                                    _vm._v("Рефералы:")
                                  ]),
                                  _vm._v(" "),
                                  _c("b-table", {
                                    attrs: {
                                      striped: "",
                                      hover: "",
                                      items: _vm.referals,
                                      fields: _vm.fields
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-row",
                    { staticClass: "mt-4" },
                    [_c("b-col", [_c("Leaderboardtop")], 1)],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-col",
            { staticClass: "mt-4 col-r", attrs: { cols: "2" } },
            [_c("Skyscrapright"), _vm._v(" "), _c("Rectright")],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);