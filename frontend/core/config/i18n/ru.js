// USA
export const locale = {
    TRANSLATOR: {
        SELECT: "Выберите ваш язык"
    },
    MENU: {
        NEW: "Новый",
        ACTIONS: "Действия",
        CREATE_POST: "Создать новый пост",
        PAGES: "Страницы",
        FEATURES: "Фичи",
        APPS: "Приложения",
        DASHBOARD: "Панель управления"
    },
    AUTH: {
        GENERAL: {
            OR: "Or",
            SUBMIT_BUTTON: "Submit",
            NO_ACCOUNT: "Don't have an account?",
            SIGNUP_BUTTON: "Sign Up",
            FORGOT_BUTTON: "Forgot Password",
            BACK_BUTTON: "Back",
            PRIVACY: "Privacy",
            LEGAL: "Legal",
            CONTACT: "Contact"
        },
        LOGIN: {
            TITLE: "Login Account",
            BUTTON: "Sign In"
        },
        FORGOT: {
            TITLE: "Forgot Password?",
            DESC: "Enter your email to reset your password",
            SUCCESS: "Your account has been successfully reset."
        },
        REGISTER: {
            TITLE: "Sign Up",
            DESC: "Enter your details to create your account",
            SUCCESS: "Your account has been successfuly registered."
        },
        INPUT: {
            EMAIL: "Email",
            FULLNAME: "Fullname",
            PASSWORD: "Password",
            CONFIRM_PASSWORD: "Confirm Password",
            USERNAME: "Username"
        },
        VALIDATION: {
            INVALID: "{{name}} is not valid",
            REQUIRED: "{{name}} is required",
            MIN_LENGTH: "{{name}} minimum length is {{min}}",
            AGREEMENT_REQUIRED: "Accepting terms & conditions are required",
            NOT_FOUND: "The requested {{name}} is not found",
            INVALID_LOGIN: "The login detail is incorrect",
            REQUIRED_FIELD: "Required field",
            MIN_LENGTH_FIELD: "Minimum field length:",
            MAX_LENGTH_FIELD: "Maximum field length:",
            INVALID_FIELD: "Field is not valid"
        }
    },
    ECOMMERCE: {
        COMMON: {
            SELECTED_RECORDS_COUNT: "Количество выбранных записей: ",
            ALL: "All",
            SUSPENDED: "Suspended",
            ACTIVE: "Active",
            FILTER: "Filter",
            BY_STATUS: "by Status",
            BY_TYPE: "by Type",
            BUSINESS: "Business",
            INDIVIDUAL: "Individual",
            SEARCH: "Search",
            IN_ALL_FIELDS: "in all fields"
        },
        ECOMMERCE: "eCommerce",
        CUSTOMERS: {
            CUSTOMERS: "Customers",
            CUSTOMERS_LIST: "Customers list",
            NEW_CUSTOMER: "New Customer",
            DELETE_CUSTOMER_SIMPLE: {
                TITLE: "Customer Delete",
                DESCRIPTION: "Are you sure to permanently delete this customer?",
                WAIT_DESCRIPTION: "Customer is deleting...",
                MESSAGE: "Customer has been deleted"
            },
            DELETE_CUSTOMER_MULTY: {
                TITLE: "Customers Delete",
                DESCRIPTION: "Are you sure to permanently delete selected customers?",
                WAIT_DESCRIPTION: "Customers are deleting...",
                MESSAGE: "Selected customers have been deleted"
            },
            UPDATE_STATUS: {
                TITLE: "Status has been updated for selected customers",
                MESSAGE: "Selected customers status have successfully been updated"
            },
            EDIT: {
                UPDATE_MESSAGE: "Customer has been updated",
                ADD_MESSAGE: "Customer has been created"
            }
        }
    },
    search: {
        filters: {
            freshness: {
                title: 'Период',
                all: 'Все',
                past_day: 'За последний день',
                past_week: 'За последнюю неделю',
                past_month: 'За последний месяц',
            },
            fileType: {
                title: 'Тип файла',
                all: 'Все',
                doc: 'doc',
                docx: 'docx',
                dwf: 'dwf',
                pdf: 'pdf',
                ppt: 'ppt',
                pptx: 'pptx',
                psd: 'psd',
                xls: 'xls',
                xlsx: 'xlsx',
            },
            safeSearch: {
                title: 'Безопасность поиска',
                off: 'Отключено',
                moderate: 'Умеренная',
                strict: 'Строгая'
            },
            aspect: {
                title: 'Форма',
                all: 'Все',
                square: 'Квадратные',
                wide: 'Широкие',
                tall: 'Вытянутые'
            },
            color: {
                title: 'Цвета',
                all: 'Все',
                colored: 'Цветные',
                monochrome: 'Черно-белые'
            },
            size: {
                title: 'Размер',
                all: 'Все',
                small: 'Маленькие',
                medium: 'Средние',
                large: 'Большие',
                wallpaper: 'Обои',
            },
            imageType: {
                title: 'Тип картинки',
                all: 'Все',
                animated: 'Анимированные',
                clipart: 'Клипарт',
                line: 'Линейные',
                photo: 'Фото',
                transparent: 'Прозрачные'
            },
            license: {
                title: 'Лицензия',
                all: 'Все',
                public: 'Публичные',
                share: 'Бесплатно для личного пользования',
                share_commercially: 'Бесплатно для использования в коммерческих целях',
                modify: 'Бесплатно для личного изменения',
                modify_commercially: 'Бесплатно для изменения в коммерческих целях',
            },
            videoLength: {
                title: 'Длительность',
                all: 'Все',
                short: 'Короткие',
                medium: 'Средние',
                long: 'Длинные',
            },
            resolution: {
                title: 'Качество',
                all: 'Все',
                '480p': '480p',
                '720p': '720p',
                '1080p': '1080p',
            },
            sortBy: {
                title: 'Сортировка',
                relevance: 'По релевантности',
                date: 'По дате'
            }
        }
    },
    main: {
        search_title: 'Искать',
        search_placeholder: 'Что будем искать?',
    },
    web_page: {
        filters: 'Фильтры',
        results_count: 'Страниц найдено по вашему запросу:',
        related_searches: 'Похожие запросы:',
        videos: 'Видео',
        web: 'Веб',
        images: 'Картинки',
        news: 'Новости',
        show_videos: 'Показать еще видео',
        hide_videos: 'Скрыть видео',
        show_images: 'Показать еще картинки',
        hide_images: 'Скрыть картинки',
        search: 'Искать'
    },
    message: {
        empty_query: 'Вы отправили пустой запрос!',
        empty_result: 'К сожалению, ничего не найдено!',
        search_limit: 'К сожалению, вы исчерпали лимит поисковых запросов на сегодня! Приходите завтра.'
    },
    navbar: {
        settings: 'НАСТРОЙКИ',
        messenger: 'МЕССЕНДЖЕР',
        mail: 'ПОЧТА',
        disk: 'ДИСК',
    },
    services: {
        all: 'ВСЕ СЕРВИСЫ SBA+',
        all_desc: 'В данный момент часть сервисов находится в разработке и недоступны для использования',
        all_services: 'ВСЕ СЕРВИСЫ',
        all_apps: 'ВСЕ ПРИЛОЖЕНИЯ',
        all_programs: 'ВСЕ ПРОГРАММЫ',
        all_biz: 'ВСЕ ДЛЯ БИЗНЕСА',
        all_services_sba: 'ПРИЛОЖЕНИЯ SBA+',
        all_services_title: 'В данном разделе собраны все приложения проекта SBA+',
        app_video: 'VIDEO+',
        app_video_descr: 'Скачать приложение VIDEO+. Видео Блог - это хостинг для размещения, хранения, поиска, аренды и продажи видеофайлов, и фильмов.',
        app_sba: 'SBA+',
        app_sba_descr: 'Скачать приложение SBA PLUS. SBA+ - это Super App приложение на котором вы можете использовать все сервисы проекта через одно приложение на телефоне.',
        app_online: 'ONLINE+',
        app_online_descr: 'Скачать приложение ONLINE+. Бизнес торговая платформа нового поколения, основанная на принципах социальных сообществ в Интернете.',
        app_photogram: 'PHOTOGRAM+',
        app_photogram_descr: 'Скачать приложение PHOTOGRAM. Сервис позволяющий легко и удобно снимать креативные фото и видео, редактировать их, а также делиться ими.',
        app_connect: 'CONNECT+',
        app_connect_descr: 'Скачать мессенджер CONNECT. Коннект - это вебсайт, программа и приложение, для обмена текстовыми сообщениями, совершать аудио и видео звонки.',
        app_mail: 'POCHTA+',
        app_mail_descr: 'Скачать почтовый сервис POCHTA+. ПОЧТА ПЛЮС - это защищённый почтовый хостинг. Антивирус, антиспам, SSL шифрование всех протоколов и без рекламы.',
        app_shortlinks: 'SHORTLINKS+',
        app_shortlinks_descr: 'Скачать приложение SHORTLINKS+. Сервисы для сокращения ссылок помогают сократить длинные url, чтобы использовать их в социальных сетях.',
        app_constr: 'VVEB+',
        app_constr_descr: 'Скачать конструктора сайтов VVEB+. Создайте бесплатно сайт, лэндинг или интернет-магазин с помощью конструктора сайтов всего за пару часов.',
        app_help: 'HELPDESK+',
        app_help_descr: 'Скачать приложение HELPDESK+. Центр поддержки - это информационная система технической поддержки, решения проблем пользователей.',
        app_disk: 'DISK+',
        app_disk_descr: 'Скачать приложение DISK+. Диск - это сервис позволяющий хранить и передавать файлы на любое устройство, подключённое к Интернету.',
        app_store: 'STORE+',
        app_store_descr: 'Скачать приложение STORE+. Store plus - это собственный магазин Android приложений с легкой навигацией и понятным интерфейсом.',

        biz_title: 'БИЗНЕС СЕРВИСЫ SBA+',
        biz_descr: 'Сервисы, котрые либо помогают продвигать Ваш бизнес или сами являются источником прибыли',
        biz_platform: 'Бизнес платформа',
        biz_platform_descr: 'Бизнес торговая платформа нового поколения, основанная на принципах социальных сообществ в Интернете',
        biz_video: 'Видео блог',
        biz_video_descr: 'Видео блог - это хостинг для размещения, хранения, поиска, аренды и продажи видеофайлов',
        biz_aladdin: 'Аладдин маркет',
        biz_aladdin_descr: 'Это электронная мультивендорная мультивалютная мультиязычная торговая площадка',
        biz_market: 'Магазин приложений',
        biz_market_descr: 'Собственный магазин Android приложений с легкой навигацией и понятным интерфейсом',
        biz_mail: 'Почтовый сервис',
        biz_mail_descr: 'Защищённый почтовый хостинг. Антивирус, антиспам, SSL шифрование всех протоколов и без рекламы',
        biz_messenger: 'Мессенджер Коннект',
        biz_messenger_descr: 'Вебсайт приложение, для обмена текстовыми сообщениями, совершать аудио и видео звонки',
        biz_shortlinks: 'Сервис коротких ссылок',
        biz_shortlinks_descr: 'Сервисы для сокращения ссылок помогают сократить длинные url, чтобы использовать их в соцсетях',
        biz_disk: 'Сервис хранения',
        biz_disk_descr: 'Сервис позволяющий хранить и передавать файлы на любое устройство, подключённое к Интернету',
        biz_webinar: 'Вебинарная комната',
        biz_webinar_descr: 'Работает по мировым стандартам безопасности, все данные копируются и резервируются',
        biz_site_constr: 'Конструктор сайтов',
        biz_site_constr_descr: 'Создайте бесплатно сайт, лэндинг или интернет-магазин с помощью конструктора сайтов всего за пару часов',
        biz_adv: 'Реклама на проекте',
        biz_adv_descr: 'Контекстная и медийная реклама поможет увеличить продажи и привлечь новых клиентов для Вашего бизнеса',
        biz_online: 'Онлайн-обучение',
        biz_online_descr: 'Онлайн образовательная платформа для увеличения узнаваемости личного бренда и увеличения доходов',
        biz_craud: 'Краудфандинг',
        biz_craud_descr: 'Краудфандинговая платформа - это площадка, используемая для размещения и продвижения проектов в Интернете',
        biz_stock: 'Биржа SBA INVEST',
        biz_stock_descr: 'Платформа с сервисом P2P биржи для Смарт Акций Компании SBA INVEST',
        biz_aladdin_stock: 'Биржа Aladdin',
        biz_aladdin_stock_descr: 'Платформа с сервисом P2P Биржи для Смарт Акций Компании Аладдин',
        biz_coin: 'Биржа SBA COIN',
        biz_coin_descr: 'Платформа с сервисом P2P Биржи для SBA COIN компании SBA INVEST',
        biz_service_started: 'Сервис запущен',
        biz_service_starting: 'Скоро запуск',
        biz_service_finish: 'Сервис приостоновлен',

        prog_title: 'ПРОГРАММЫ SBA+',
        prog_descr: 'В данном разделе собраны все программы для скачивания и использования на Windows',
        prog_progress: 'В разработке',
        prog_progress_descr: 'Страница находится в разработке',

        service_title: 'Бизнес платформа',
        service_descr: 'Бизнес торговая платформа нового поколения, основанная на принципах социальных сообществ в Интернете',
        service_video: 'Видео блог',
        service_video_descr: 'Видео блог - это хостинг для размещения, хранения, поиска, аренды и продажи видеофайлов',
        service_market: 'Аладдин маркет',
        service_market_descr: 'Это электронная мультивендорная мультивалютная мультиязычная торговая площадка',
        service_app_shop: 'Магазин приложений',
        service_app_shop_descr: 'Собственный магазин Android приложений с легкой навигацией и понятным<br> интерфейсом',
        service_mail: 'Почтовый сервис',
        service_mail_descr: 'Защищённый почтовый хостинг. Антивирус, антиспам, SSL шифрование всех протоколов и без рекламы',
        service_disk: 'Сервис хранения',
        service_disk_descr: 'Сервис позволяющий хранить и передавать файлы на любое устройство, подключённое к Интернету',
        service_messegger: 'Мессенджер Коннект',
        service_messegger_descr: 'Вебсайт, программа и приложение, для обмена текстовыми, аудио и видео сообщениями',
        service_link: 'Сервис коротких ссылок',
        service_link_descr: 'Сервисы для сокращения ссылок помогают сократить длинные url, чтобы использовать их в соцсетях',
        service_soc_net: 'Социальная сеть',
        service_soc_net_descr: 'Платформа, онлайн сервис или веб-сайт, предназначенные для построения и организации социальных взаимоотношений',
        service_webinar: 'Вебинарная комната',
        service_webinar_descr: 'Работает по мировым стандартам безопасности, все данные копируются и резервируются',
        service_site_constr: 'Конструктор сайтов',
        service_site_constr_descr: 'Создайте бесплатно сайт, лэндинг или интернет-магазин с помощью конструктора сайтов всего за пару часов',
        service_photo: 'ФОТОГРАМ',
        service_photo_descr: 'Сервис позволяющий легко и удобно снимать креативные фото и видео, редактировать их, а также делиться ими',
        service_star_map: 'Карта звёздного неба',
        service_star_map_descr: 'Изображение звёздного неба или его части, показывающего расположенные на нём объекты звёзды, планеты, кометы',
        service_adv: 'Реклама на проекте',
        service_adv_descr: 'Контекстная и медийная реклама поможет увеличить продажи и привлечь новых клиентов для Вашего бизнеса',
        service_search: 'Поисковый сервис',
        service_search_descr: 'Система алгоритмов, реализующая поиск необходимой Вам информации в <br>Интернете',
        service_teach: 'Онлайн-обучения',
        service_teach_descr: 'Онлайн образовательная платформа для увеличения узнаваемости личного бренда и увеличения доходов',
        service_craud: 'Краудфандинг',
        service_craud_descr: 'Краудфандинговая платформа - это площадка, используемая для размещения и продвижения проектов в Интернете',
        service_support: 'Служба поддержки',
        service_support_descr: 'Техподдержка, посредством которой, предприятия и организации обеспечивают помощь пользователям',
        service_started: 'Сервис запущен',
        service_starting: 'Скоро запуск',
        service_finish: 'Сервис приостоновлен',



    },
    box_menu: {
        business_br: 'Бизнес<br>онлайн',
        business: 'Бизнес онлайн',
        app_shop_br: 'Магазин<br>приложений',
        app_shop: 'Магазин приложений',
        star_map_br: 'Карта<br>звёзд',
        star_map: 'Карта звёзд',
        photogram_br: 'Фото<br>Грам',
        photogram: 'Социальная сеть',
        video_br: 'Видео<br>блог',
        video: 'Видео блог',
        webinar_br: 'Вебинарная<br>комната',
        webinar: 'Вебинарная комната',
        short_links_br: 'Короткие<br>ссылки',
        short_links: 'Короткие ссылки',
        site_constr_br: 'Конструктор<br>сайтов',
        site_constr: 'Конструктор сайтов',
        shop_br: 'Интернет<br>магазин',
        shop: 'Интернет магазин',
        more: 'Больше',
        mail: 'ПОЧТА+',
        more_m: 'Ещё'
    },
    currencies: {
        title: 'Курсы валют ЦБ РФ на сегодня',
    },
    footer: {
        adv: 'Реклама',
        all_services: 'Все сервисы',
        help: 'Помощь',
        rules_1: 'Пользовательское соглашение',
        rules_2: 'Политика конфиденциальности',
    },
    sidebar: {
        business: 'Бизнес онлайн',
        video: 'Видео блог',
        market: 'Аладдин маркет',
        app_shop: 'Магазин приложений',
        mail: 'Почтовый сервис',
        disk: 'Сервис хранения',
        messenger: 'Мессенджер Коннект',
        short_links: 'Сервис коротких ссылок',
        webinar: 'Вебинарная комната',
        site_constr: 'Конструктор сайтов',
        photo: 'ФОТОГРАМ',
        soc_net: 'Социальная сеть',
        star_map: 'Карта звездного неба',
        adv: 'Разместить рекламу',
        help: 'Служба поддержки',
        all_services: 'Все сервисы'
    },
    agreement: {
        main_page: '« Главная страница',
        title: 'Пользовательское соглашение',
        text: 'Настоящее Пользовательское Соглашение (Далее Соглашение) регулирует отношения между АО Сухба (далее Поиск или Администрация) с одной стороны и пользователем сайта с другой.<br>\n' +
            '              Сайт Поиск не является средством массовой информации.<br>\n' +
            '              <br>\n' +
            '              Используя сайт, Вы соглашаетесь с условиями данного соглашения.<br>\n' +
            '              Если Вы не согласны с условиями данного соглашения, не используйте сайт Поиск!<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Права и обязанности сторон<br>\n' +
            '              Пользователь имеет право:<br>\n' +
            '              - осуществлять поиск информации на сайте<br>\n' +
            '              - получать информацию на сайте<br>\n' +
            '              - копировать информацию на другие сайты с указанием источника<br>\n' +
            '              - копировать информацию на другие сайты с разрешения правообладателя<br>\n' +
            '              - требовать от администрации скрытия любой информации о пользователе<br>\n' +
            '              - использовать информацию сайта в личных некоммерческих целях<br>\n' +
            '              <br>\n' +
            '              Администрация имеет право:<br>\n' +
            '              - по своему усмотрению и необходимости создавать, изменять, отменять правила<br>\n' +
            '              - ограничивать доступ к любой информации на сайте<br>\n' +
            '              - создавать, изменять, удалять информацию<br>\n' +
            '              <br>\n' +
            '              Пользователь обязуется:<br>\n' +
            '              - не нарушать работоспособность сайта<br>\n' +
            '              - не использовать скрипты (программы) для автоматизированного сбора информации и/или взаимодействия с Сайтом и его Сервисами<br>\n' +
            '              <br>\n' +
            '              Администрация обязуется:<br>\n' +
            '              - поддерживать работоспособность сайта за исключением случаев, когда это невозможно по независящим от Администрации причинам.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Ответственность сторон<br>\n' +
            '              - администрация не несет никакой ответственности за достоверность информации, скопированной из других источников<br>\n' +
            '              - администрация не несет никакой ответственности за услуги, предоставляемые третьими лицами<br>\n' +
            '              - в случае возникновения форс-мажорной ситуации (боевые действия, чрезвычайное положение, стихийное бедствие и т. д.) Администрация не гарантирует сохранность информации, размещённой Пользователем, а также бесперебойную работу информационного ресурса<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Условия действия Соглашения<br>\n' +
            '              Данное Соглашение вступает в силу при любом использовании данного сайта.<br>\n' +
            '              Соглашение перестает действовать при появлении его новой версии.<br>\n' +
            '              Администрация оставляет за собой право в одностороннем порядке изменять данное соглашение по своему усмотрению.<br>\n' +
            '              Администрация не оповещает пользователей об изменении в Соглашении.<br>'
    },
    privacy_policy: {
        main_page: '« Главная страница',
        title: 'Политика конфиденциальности',
        text: 'Политика обработки персональных данных (далее - Политика или Политика конфиденциальности) разработана в соответствии с Федеральным законом от 27.07.2006. №152-ФЗ "О персональных данных" (далее - ФЗ-152).<br>\n' +
            '              <br>\n' +
            '              Настоящая Политика определяет порядок обработки персональных данных и меры по обеспечению безопасности персональных данных у владельца сайта АО Сухба (далее Поиск или Оператор).<br>\n' +
            '              <br>\n' +
            '              Целью настоящей политики конфиденциальности является обеспечение защиты прав и свобод человека и гражданина при обработке его персональных данных, в том числе защиты прав на неприкосновенность частной жизни, личную и семейную тайну.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Если Вы не согласны с условиями нашей Политики конфиденциальности, не используйте сайт www.sba.plus! <br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Настоящая Политика содержит следующую информацию:<br>\n' +
            '              <br>\n' +
            '              - цель обработки персональных данных; <br>\n' +
            '              - перечень персональных данных, на обработку которых дается согласие субъекта персональных данных; <br>\n' +
            '              - наименование или фамилия, имя, отчество и адрес лица, осуществляющего обработку персональных данных по поручению оператора, если обработка будет поручена такому лицу; <br>\n' +
            '              - перечень действий с персональными данными, на совершение которых дается согласие, общее описание используемых оператором способов обработки персональных данных; <br>\n' +
            '              - срок, в течение которого действует согласие субъекта персональных данных, а также способ его отзыва, если иное не установлено федеральным законом;<br>\n' +
            '              - информацию о том, как можно отозвать свое согласие на обработку персональных данных. <br>\n' +
            '              <br>\n' +
            '              Основные понятия, используемые в настоящей политике конфиденциальности:<br>\n' +
            '              <br>\n' +
            '              Персональные данные - любая информация, относящаяся к прямо или косвенно определенному или определяемому физическому лицу (субъекту персональных данных); <br>\n' +
            '              Оператор - государственный орган, муниципальный орган, юридическое или физическое лицо, самостоятельно или совместно с другими лицами организующие и (или) осуществляющие обработку персональных данных, а также определяющие цели обработки персональных данных, состав персональных данных, подлежащих обработке, действия (операции), совершаемые с персональными данными; <br>\n' +
            '              Обработка персональных данных - любое действие (операция) или совокупность действий (операций), совершаемых с использованием средств автоматизации или без использования таких средств с персональными данными, включая сбор, запись, систематизацию, накопление, хранение, уточнение (обновление, изменение), извлечение, использование, передачу (распространение, предоставление, доступ), обезличивание, блокирование, удаление, уничтожение персональных данных;<br>\n' +
            '              <br>\n' +
            '              Принципы сбора персональных данных:<br>\n' +
            '              <br>\n' +
            '              - Обработка персональных данных осуществляется на законной и справедливой основе <br>\n' +
            '              - Обработка персональных данных ограничивается достижением конкретных, заранее определенных и законных целей. <br>\n' +
            '              - Не допускается объединение баз данных, содержащих персональные данные, обработка которых осуществляется в целях, несовместимых между собой. <br>\n' +
            '              - Обработке подлежат только персональные данные, которые отвечают целям их обработки. <br>\n' +
            '              - Содержание и объем обрабатываемых персональных данных соответствуют заявленным целям обработки. <br>\n' +
            '              - При обработке персональных данных обеспечиваются точность персональных данных, их достаточность, а в необходимых случаях и актуальность по отношению к целям обработки персональных данных. <br>\n' +
            '              - Оператор принимает необходимые меры либо обеспечивает их принятие по удалению или уточнению неполных или неточных данных. <br>\n' +
            '              - Хранение персональных данных осуществляется в форме, позволяющей определить субъекта персональных данных, не дольше, чем этого требуют цели обработки персональных данных, если срок хранения персональных данных не установлен федеральным законом, договором, стороной которого, выгодоприобретателем или поручителем по которому является субъект персональных данных. Обрабатываемые персональные данные подлежат уничтожению либо обезличиванию по достижении целей обработки или в случае утраты необходимости в достижении этих целей, если иное не предусмотрено федеральным законом. <br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Перечень собираемых персональных данных<br>\n' +
            '              Оператор не собирает никакой личной информации о пользователе. Однако, личную информацию пользователя могут собирать внешние организации (третьи стороны), предоставляющие инструменты для сбора информации о состоянии сайта. По этой причине, Оператор снимает с себя ответственность по сохранению конфиденциальности данных пользователя. С Условиями конфиденциальности третьих сторон, пользователь может ознакомиться на их сайтах.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Прочая информация<br>\n' +
            '              <br>\n' +
            '              - Редакция Политики конфиденциальности от 28-12-2018 <br>\n' +
            '              - Срок действия настоящей Политики конфиденциальности не ограничен <br>\n' +
            '              - Настоящая Политика конфиденциальности теряет свою силу при появлении новой версии, размещенной на сайте https://www.sba.plus/ <br>\n' +
            '              - Дети любых возрастов могут беспрепятственно пользоваться данным сайтом.<br>'
    }
};
