<?php

namespace App\Models;

use Dirape\Token\Token;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * @property int id
 * @property int user_id
 * @property string number
 * @property string code
 * @property int win
 * @property double cash
 * @property int withdrawed
 * @property int created_at
 * @property int updated_at
 * @package App\Models
 */

class Ticket extends Model
{
    protected $model = Ticket::class;

    protected $table = 'tickets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'number',
        'code',
        'win',
        'cash',
        'withdrawed'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public static function createTicket(User $user)
    {
        $ticket = new Ticket();
        $ticket->user_id = $user->id;
        $ticket->number = Ticket::randomNumber(10);
        $ticket->code = (new Token)->RandomString(12);
        $ticket->save();
        return $ticket;
    }

    private static function randomNumber($length) {
        $result = '';
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        while (Ticket::where('number', $result)->first()) {
            $result = '';
            for($i = 0; $i < $length; $i++) {
                $result .= mt_rand(0, 9);
            }
        }
        return $result;
    }
}
