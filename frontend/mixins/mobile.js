/**
 * Mixin определения устройства пользователя
 */

export const mobileMixins = {
    methods: {
        isMobileVer: function() {
            return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
        },
    },
};
