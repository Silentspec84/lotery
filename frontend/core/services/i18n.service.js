const i18nService =
    {
        defaultLanguage: "en",
        languages:
        [
            {
                lang: "en",
                name: "English",
                flag: "/images/icons/svg/flags/226-united-states.svg"
            },
            {
                lang: "ch",
                name: "Mandarin",
                flag: "/images/icons/svg/flags/034-china.svg"
            },
            {
                lang: "es",
                name: "Spanish",
                flag: "/images/icons/svg/flags/128-spain.svg"
            },
            {
                lang: "jp",
                name: "Japanese",
                flag: "/images/icons/svg/flags/063-japan.svg"
            },
            {
                lang: "de",
                name: "German",
                flag: "/images/icons/svg/flags/162-germany.svg"
            },
            {
                lang: "fr",
                name: "French",
                flag: "/images/icons/svg/flags/195-france.svg"
            },
            {
                lang: "fr",
                name: "French",
                flag: "/images/icons/svg/flags/195-france.svg"
            },
            {
                lang: "ru",
                name: "Russian",
                flag: "/images/icons/svg/flags/248-russia.svg"
            }
        ],

        /**
         * Keep the active language in the localStorage
         * @param lang
         */
        setActiveLanguage(lang)
        {
            localStorage.setItem("language", lang);
        },

        /**
         * Get the current active language
         * @returns {string | string}
         */
        getActiveLanguage() {
            return localStorage.getItem("language") || this.defaultLanguage;
        }
    };

export default i18nService;
