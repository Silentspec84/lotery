import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

import mainRoutes from "./modules/main";

export const constantRoutes = [
    mainRoutes,
    {
        path: "*",
        redirect: "/404"
    },
    {
        // the 404 route, when none of the above matches
        path: "/404",
        name: "404",
        component: () => import("@/views/error/index")
    }
];

const createRouter = () => new Router({
    hashbang: false,
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    base: process.env.MIX_LARAVUE_PATH,
    routes: constantRoutes,
});

const router = createRouter();

export function resetRouter() {
    const newRouter = createRouter();
    router.matcher = newRouter.matcher; // reset router
}

export default router;
