import jQuery from "jquery";

/**
 * Mixin для управления сайдбаром
 */

export const sidebarMixins = {
    methods: {
        sidebarShow: function() {
            let panel = jQuery('#kt_quick_panel');
            if (panel.hasClass('offcanvas-on')) {
                panel.removeClass('offcanvas-on');
                panel.addClass('offcanvas-off');
            } else {
                panel.removeClass('offcanvas-off');
                panel.addClass('offcanvas-on');
            }
        },
        sidebarHide: function() {
            let panel = jQuery('#kt_quick_panel');
            if (panel.hasClass('offcanvas-on')) {
                panel.removeClass('offcanvas-on');
                panel.addClass('offcanvas-off');
            }
        },
    },
};
