(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[27],{

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/assets/sass/themes/layout/header/base/light.scss":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./resources/js/src/assets/sass/themes/layout/header/base/light.scss ***!
  \**************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "@media (min-width: 992px) {\n  .header {\n    background-color: #ffffff;\n  }\n  .header-fixed .header {\n    box-shadow: 0px 0px 40px 0px rgba(82, 63, 105, 0.1);\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link {\n    border-radius: 4px;\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link .menu-text {\n    color: #6c7293;\n    font-weight: 500;\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link .menu-arrow {\n    color: #6c7293;\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link .menu-icon {\n    color: #6c7293;\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #6c7293;\n  }\n  .header .header-menu .menu-nav > .menu-item > .menu-link svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link, .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link {\n    background-color: rgba(77, 89, 149, 0.06);\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link .menu-text, .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link .menu-text {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link .menu-arrow, .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link .menu-arrow {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link .menu-icon, .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link .menu-icon {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link svg g [fill], .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item.menu-item-here > .menu-link svg:hover g [fill], .header .header-menu .menu-nav > .menu-item.menu-item-active > .menu-link svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link, .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link {\n    background-color: rgba(77, 89, 149, 0.06);\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-text, .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-text {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-hor-arrow, .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-hor-arrow {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-icon, .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link .menu-icon {\n    color: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link svg g [fill], .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #3699FF;\n  }\n  .header .header-menu .menu-nav > .menu-item:hover:not(.menu-item-here):not(.menu-item-active) > .menu-link svg:hover g [fill], .header .header-menu .menu-nav > .menu-item.menu-item-hover:not(.menu-item-here):not(.menu-item-active) > .menu-link svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n}\n@media (max-width: 991.98px) {\n  .topbar {\n    background-color: #ffffff;\n    box-shadow: none;\n  }\n  .topbar-mobile-on .topbar {\n    box-shadow: 0px 0px 40px 0px rgba(82, 63, 105, 0.2);\n    border-top: 1px solid #eff0f6;\n  }\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./resources/js/src/assets/sass/themes/layout/header/base/light.scss":
/*!***************************************************************************!*\
  !*** ./resources/js/src/assets/sass/themes/layout/header/base/light.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../../../node_modules/css-loader/dist/cjs.js!../../../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!./light.scss */ "./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/assets/sass/themes/layout/header/base/light.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);