export default {
    namespaced: true,
    state: {
        user: null,
        ref_link: null,
    },
    getters: {
        user(state) {
            return state.user;
        },
        ref_link(state) {
            return state.ref_link;
        }
    },
    mutations: {
        setUser(state, user) {
            state.user = user;
        },
        setRefLink(state, ref_link) {
            state.ref_link = ref_link;
        },
    },
    actions: {
        updateUser(context, payload) {
            context.commit('setUser', payload);
        },
        updateRefLink(context, payload) {
            context.commit('setRefLink', payload);
        },
    },
};
