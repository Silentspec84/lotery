<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @property int id
 * @property string email
 * @property int email_verified_at
 * @property string password
 * @property string remember_token
 * @property string role
 * @property double balance
 * @property int lotteries
 * @property string ref_link
 * @property int referer_id
 * @property int is_fake
 * @property int tickets_remain
 * @property int created_at
 * @property int updated_at
 * @property int last_login
 * @package App\Models
 */

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';

    protected $table = 'users';

    protected $model = User::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'ref_link',
        'role',
        'referer_id',
        'balance',
        'lotteries',
        'is_fake',
        'tickets_remain'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'last_login' => 'datetime',
    ];
}
