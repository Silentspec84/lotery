export default {
    namespaced: true,
    state: {
        advs: null,
    },
    getters: {
        advs(state) {
            return state.advs;
        }
    },
    mutations: {
        setAdvs(state, advs) {
            state.advs = advs;
        },
    },
    actions: {
        updateAdvs(context, payload) {
            context.commit('setAdvs', payload);
        },
    },
};
