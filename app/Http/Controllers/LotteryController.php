<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use App\Models\Ticket;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LotteryController extends Controller
{
    public function getTicket(Request $request)
    {
        $user = Auth::guard()->user();
        if ($user->tickets_remain > 0) {
            $user->tickets_remain -= 1;
            $user->save();
            $ticket = Ticket::createTicket($user);
            return response()->json(['success' => true, 'ticket' => $ticket]);
        } else {
            return response()->json(['success' => false, 'message' => 'Лимит исчерпан, на сегодня больше нет билетов!']);
        }
    }

    public function getYesturdayWinTickets(Request $request)
    {
        $from_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->modify('-1 day')->getTimestamp());
        $to_date = date('Y-m-d H:i:s', (new DateTime('now 00:00:00'))->getTimestamp());
        $ticket_1 = Ticket::where('win', 1)->where('level', 1)->whereBetween('created_at', [$from_date, $to_date])->first();
        $tickets_2 = Ticket::where('win', 1)->where('level', 2)->whereBetween('created_at', [$from_date, $to_date])->get();

        $real_participants = Ticket::whereBetween('created_at', [$from_date, $to_date])->distinct()->count('user_id');
        $adv_price = Settings::where('name', 'adv_price')->first()->value;
        $yesturday_fond = $real_participants / 1000 * $adv_price - 0.1 * $real_participants / 1000 * $adv_price;

        $first_place = Settings::where('name', 'first_place')->first()->value;
        $first_place_val = $yesturday_fond * $first_place / 100 ?? 0;
        $second_place = Settings::where('name', 'second_place')->first()->value;
        $second_place_val = $yesturday_fond * $second_place / 100 / 10 ?? 0;
        $third_place = Settings::where('name', 'third_place')->first()->value;
        $third_place_val = $yesturday_fond * $third_place / 100 / 100 ?? 0;

        foreach ($tickets_2 as $ticket_2) {
            $tickets[] = $ticket_2->number;
            if (count($tickets) === 2) {
                $second[] = ['1' => $tickets[0], '2' => $tickets[1]];
                $tickets = [];
            }
        }
        $tickets_3 = Ticket::where('win', 1)->where('level', 3)->whereBetween('created_at', [$from_date, $to_date])->get();
        foreach ($tickets_3 as $ticket_3) {
            $tickets[] = $ticket_3->number;
            if (count($tickets) === 10) {
                $third[] = [
                    '1' => $tickets[0], '2' => $tickets[1], '3' => $tickets[2], '4' => $tickets[3], '5' => $tickets[4],
                    '6' => $tickets[5], '7' => $tickets[6], '8' => $tickets[7], '9' => $tickets[8], '10' => $tickets[9],
                ];
                $tickets = [];
            }
        }

        return response()->json([
            'success' => true,
            'tickets' => [
                'first' => $ticket_1->number,
                'first_val' => round($first_place_val, 2) . "$",
                'second' => $second,
                'second_val' => round($second_place_val, 2) . "$",
                'third' => $third,
                'third_val' => round($third_place_val, 2) . "$",
            ]
        ], 200);
    }

    public function checkTicket(Request $request)
    {
        $user_id = Auth::guard()->id();
        $number = $request->post('number');
        $ticket = Ticket::where('number', $number)->first();
        if (empty($ticket)) {
            return response()->json([
                'success' => false,
                'message' => 'Номер билета не найден. Возможно, тут какая-то ошибка.'
            ], 200);
        }
        if ($ticket->user_id !== $user_id) {
            return response()->json([
                'success' => true,
                'message' => 'Бро, ты уж извини, но это не твой билет. У нас все ходы записаны!'
            ], 200);
        }
        if ($ticket->win !== 1) {
            return response()->json([
                'success' => true,
                'message' => 'К сожалению, сегодня удача отвернулась от тебя. Проверь еще один билет или попытай удачу в новом розыгрыше!'
            ], 200);
        }
        if ($ticket->withdrawed === 1) {
            return response()->json([
                'success' => true,
                'message' => 'Этот билет уже был обналичен!'
            ], 200);
        }
        return response()->json(['success' => true], 200);
    }

    public function getWin(Request $request)
    {
        $code = $request->post('code');
        $user = Auth::guard()->user();
        $ticket = Ticket::where('code', $code)->where('user_id', $user->id)->first();
        $user->balance += $ticket->cash;
        $user->save();
        $ticket->withdrawed = 1;
        $ticket->save();
        return response()->json(['success' => true, 'cash' => round($ticket->cash, 2)], 200);
    }

    private function randomNumber($length) {
        $result = '';
        for($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }
        while (Ticket::where('number', $result)->first()) {
            $result = '';
            for($i = 0; $i < $length; $i++) {
                $result .= mt_rand(0, 9);
            }
        }
        return $result;
    }

}
