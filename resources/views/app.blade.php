<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="referrer" content="origin">
        <meta property="og:url" content="https://sba.plus">
        <meta property="og:type" content="website">
        <meta property="og:site_name" content="SBA+">
        <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        <meta property="og:locale" content="ru_RU">
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet"/>
        <meta name="a.validate.01" content="362adb6a1cad166c47d23f457e53b696d89f" />
    </head>
    <body style="background: #c7b39b url(/images/wp/bg-2.jpg); background-size: cover;">
        <div id="app"></div>
        <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
    </body>
</html>
