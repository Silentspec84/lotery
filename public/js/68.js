(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[68],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_content_CodePreview_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/view/content/CodePreview.vue */ "./resources/js/src/view/content/CodePreview.vue");
/* harmony import */ var _core_services_store_breadcrumbs_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/core/services/store/breadcrumbs.module */ "./resources/js/src/core/services/store/breadcrumbs.module.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      code1: {
        html: "<div>\n    <b-progress :value=\"value\" :max=\"max\" show-progress animated></b-progress>\n    <b-progress class=\"mt-2\" :max=\"max\" show-value>\n      <b-progress-bar :value=\"value * (6 / 10)\" variant=\"success\"></b-progress-bar>\n      <b-progress-bar :value=\"value * (2.5 / 10)\" variant=\"warning\"></b-progress-bar>\n      <b-progress-bar :value=\"value * (1.5 / 10)\" variant=\"danger\"></b-progress-bar>\n    </b-progress>\n\n    <b-button class=\"mt-3\" @click=\"randomValue\">Click me</b-button>\n  </div>",
        js: "export default {\n    data() {\n      return {\n        value: 45,\n        max: 100\n      }\n    },\n    methods: {\n      randomValue() {\n        this.value = Math.random() * this.max\n      }\n    }\n  }",
        value: 45,
        max: 100
      },
      code2: {
        html: "<div>\n    <h5>No label</h5>\n    <b-progress :value=\"value\" :max=\"max\" class=\"mb-3\"></b-progress>\n\n    <h5>Value label</h5>\n    <b-progress :value=\"value\" :max=\"max\" show-value class=\"mb-3\"></b-progress>\n\n    <h5>Progress label</h5>\n    <b-progress :value=\"value\" :max=\"max\" show-progress class=\"mb-3\"></b-progress>\n\n    <h5>Value label with precision</h5>\n    <b-progress :value=\"value\" :max=\"max\" :precision=\"2\" show-value class=\"mb-3\"></b-progress>\n\n    <h5>Progress label with precision</h5>\n    <b-progress :value=\"value\" :max=\"max\" :precision=\"2\" show-progress class=\"mb-3\"></b-progress>\n  </div>",
        js: "export default {\n    data() {\n      return {\n        value: 33.333333333,\n        max: 50\n      }\n    }\n  }",
        value: 33.333333333,
        max: 50
      },
      code3: {
        html: "<div>\n" + "    <h5>Custom label via default slot</h5>\n" + '    <b-progress :max="max" height="2rem">\n' + '      <b-progress-bar :value="value">\n' + "        Progress: <strong>{{ value.toFixed(2) }} / {{ max }}</strong>\n" + "      </b-progress-bar>\n" + "    </b-progress>\n" + "\n" + '    <h5 class="mt-3">Custom label via property</h5>\n' + '    <b-progress :max="max">\n' + '      <b-progress-bar :value="value" :label="`${((value / max) * 100).toFixed(2)}%`"></b-progress-bar>\n' + "    </b-progress>\n" + "\n" + '    <h5 class="mt-3">Custom label via property (HTML support)</h5>\n' + '    <b-progress :max="max">\n' + '      <b-progress-bar :value="value" :label-html="`<del>${value}</del>`"></b-progress-bar>\n' + "    </b-progress>\n" + "  </div>",
        js: "export default {\n    data() {\n      return {\n        value: 33.333333333,\n        max: 50\n      }\n    }\n  }",
        value: 33.333333333,
        max: 50
      },
      code4: {
        html: "<div>\n    <h5>Default width</h5>\n    <b-progress :value=\"value\" class=\"mb-3\"></b-progress>\n\n    <h5>Custom widths</h5>\n    <b-progress :value=\"value\" class=\"w-75 mb-2\"></b-progress>\n    <b-progress :value=\"value\" class=\"w-50 mb-2\"></b-progress>\n    <b-progress :value=\"value\" class=\"w-25\"></b-progress>\n  </div>",
        js: "export default {\n    data() {\n      return {\n        value: 75\n      }\n    }\n  }",
        value: 75
      },
      code5: {
        html: "<div>\n    <div v-for=\"bar in bars\" class=\"row mb-1\">\n      <div class=\"col-sm-2\">{{ bar.variant }}:</div>\n      <div class=\"col-sm-10 pt-1\">\n        <b-progress :value=\"bar.value\" :variant=\"bar.variant\" :key=\"bar.variant\"></b-progress>\n      </div>\n    </div>\n  </div>",
        js: "export default {\n    data() {\n      return {\n        bars: [\n          { variant: 'success', value: 75 },\n          { variant: 'info', value: 75 },\n          { variant: 'warning', value: 75 },\n          { variant: 'danger', value: 75 },\n          { variant: 'primary', value: 75 },\n          { variant: 'secondary', value: 75 },\n          { variant: 'dark', value: 75 }\n        ],\n        timer: null\n      }\n    },\n    mounted() {\n      this.timer = setInterval(() => {\n        this.bars.forEach(bar => (bar.value = 25 + Math.random() * 75))\n      }, 2000)\n    },\n    beforeDestroy() {\n      clearInterval(this.timer)\n      this.timer = null\n    }\n  }",
        bars: [{
          variant: "success",
          value: 75
        }, {
          variant: "info",
          value: 75
        }, {
          variant: "warning",
          value: 75
        }, {
          variant: "danger",
          value: 75
        }, {
          variant: "primary",
          value: 75
        }, {
          variant: "secondary",
          value: 75
        }, {
          variant: "dark",
          value: 75
        }],
        timer: null
      }
    };
  },
  components: {
    KTCodePreview: _view_content_CodePreview_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    randomValue: function randomValue() {
      this.code1.value = Math.random() * this.code1.max;
    }
  },
  beforeDestroy: function beforeDestroy() {
    clearInterval(this.code5.timer);
    this.code5.timer = null;
  },
  mounted: function mounted() {
    var _this = this;

    this.$store.dispatch(_core_services_store_breadcrumbs_module__WEBPACK_IMPORTED_MODULE_1__["SET_BREADCRUMB"], [{
      title: "Vue Bootstrap",
      route: "alert"
    }, {
      title: "Progress"
    }]);
    this.code5.timer = setInterval(function () {
      _this.code5.bars.forEach(function (bar) {
        return bar.value = 25 + Math.random() * 75;
      });
    }, 2000);
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "b-alert",
        {
          staticClass:
            "alert alert-custom alert-white alert-shadow fade show gutter-b",
          attrs: { show: "", variant: "light" }
        },
        [
          _c("div", { staticClass: "alert-icon" }, [
            _c(
              "span",
              { staticClass: "svg-icon svg-icon-lg" },
              [
                _c("inline-svg", {
                  attrs: { src: "media/svg/icons/Tools/Compass.svg" }
                })
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "alert-text" }, [
            _c("b", [_vm._v("Progress")]),
            _vm._v(
              " Use our custom progress component for displaying simple\n      or complex progress bars, featuring support for horizontally stacked\n      bars, animated backgrounds, and text labels.\n      "
            ),
            _c(
              "a",
              {
                staticClass: "font-weight-bold",
                attrs: {
                  href: "https://bootstrap-vue.js.org/docs/components/progress",
                  target: "_blank"
                }
              },
              [_vm._v("\n        See documentation.\n      ")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-md-12" },
          [
            _c("KTCodePreview", {
              attrs: { title: "Progress" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c(
                        "div",
                        [
                          _c("b-progress", {
                            attrs: {
                              value: _vm.code1.value,
                              max: _vm.code1.max,
                              "show-progress": "",
                              animated: ""
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "b-progress",
                            {
                              staticClass: "mt-2",
                              attrs: { max: _vm.code1.max, "show-value": "" }
                            },
                            [
                              _c("b-progress-bar", {
                                attrs: {
                                  value: _vm.code1.value * (6 / 10),
                                  variant: "success"
                                }
                              }),
                              _vm._v(" "),
                              _c("b-progress-bar", {
                                attrs: {
                                  value: _vm.code1.value * (2.5 / 10),
                                  variant: "warning"
                                }
                              }),
                              _vm._v(" "),
                              _c("b-progress-bar", {
                                attrs: {
                                  value: _vm.code1.value * (1.5 / 10),
                                  variant: "danger"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mt-3",
                              on: { click: _vm.randomValue }
                            },
                            [_vm._v("Click me")]
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code1.html) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code1.js) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Labels" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Add labels to your progress bars by either enabling\n            "
                        ),
                        _c("code", [_vm._v("show-progress")]),
                        _vm._v(" (percentage of max) or\n            "),
                        _c("code", [_vm._v("show-value")]),
                        _vm._v(
                          " for the current absolute value. You may\n            also set the precision (number of digits after the decimal) via\n            the "
                        ),
                        _c("code", [_vm._v("precision")]),
                        _vm._v(
                          " prop (default is 0digits after the\n            decimal).\n          "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c("h5", [_vm._v("No label")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: {
                              value: _vm.code2.value,
                              max: _vm.code2.max
                            }
                          }),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Value label")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: {
                              value: _vm.code2.value,
                              max: _vm.code2.max,
                              "show-value": ""
                            }
                          }),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Progress label")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: {
                              value: _vm.code2.value,
                              max: _vm.code2.max,
                              "show-progress": ""
                            }
                          }),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Value label with precision")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: {
                              value: _vm.code2.value,
                              max: _vm.code2.max,
                              precision: 2,
                              "show-value": ""
                            }
                          }),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Progress label with precision")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: {
                              value: _vm.code2.value,
                              max: _vm.code2.max,
                              precision: 2,
                              "show-progress": ""
                            }
                          })
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code2.html) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code2.js) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Custom progress label" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Need more control over the label? Provide your own label by using\n            the default slot within a\n            "
                        ),
                        _c("code", [
                          _vm._v(
                            "<b-progress-bar> sub-component, or by using the label or\n              label-html property on <b-progress-bar>"
                          )
                        ]),
                        _vm._v(":\n          ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c("h5", [_vm._v("Custom label via default slot")]),
                          _vm._v(" "),
                          _c(
                            "b-progress",
                            { attrs: { max: _vm.code3.max, height: "2rem" } },
                            [
                              _c(
                                "b-progress-bar",
                                { attrs: { value: _vm.code3.value } },
                                [
                                  _vm._v(
                                    "\n                Progress:\n                "
                                  ),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(_vm.code3.value.toFixed(2)) +
                                        " / " +
                                        _vm._s(_vm.code3.max)
                                    )
                                  ])
                                ]
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("h5", { staticClass: "mt-3" }, [
                            _vm._v("Custom label via property")
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-progress",
                            { attrs: { max: _vm.code3.max } },
                            [
                              _c("b-progress-bar", {
                                attrs: {
                                  value: _vm.code3.value,
                                  label:
                                    (
                                      (_vm.code3.value / _vm.code3.max) *
                                      100
                                    ).toFixed(2) + "%"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("h5", { staticClass: "mt-3" }, [
                            _vm._v("Custom label via property (HTML support)")
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-progress",
                            { attrs: { max: _vm.code3.max } },
                            [
                              _c("b-progress-bar", {
                                attrs: {
                                  value: _vm.code3.value,
                                  "label-html":
                                    "<del>" + _vm.code3.value + "</del>"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code3.html) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code3.js) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Width and height" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c(
                        "div",
                        [
                          _c("h5", [_vm._v("Default width")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "mb-3",
                            attrs: { value: _vm.code4.value }
                          }),
                          _vm._v(" "),
                          _c("h5", [_vm._v("Custom widths")]),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "w-75 mb-2",
                            attrs: { value: _vm.code4.value }
                          }),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "w-50 mb-2",
                            attrs: { value: _vm.code4.value }
                          }),
                          _vm._v(" "),
                          _c("b-progress", {
                            staticClass: "w-25",
                            attrs: { value: _vm.code4.value }
                          })
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code4.html) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code4.js) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Solid background variants" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c(
                        "div",
                        _vm._l(_vm.code5.bars, function(bar, i) {
                          return _c(
                            "div",
                            { key: i, staticClass: "row mb-1" },
                            [
                              _c("div", { staticClass: "col-sm-2" }, [
                                _vm._v(_vm._s(bar.variant) + ":")
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-10 pt-1" },
                                [
                                  _c("b-progress", {
                                    key: bar.variant,
                                    attrs: {
                                      value: bar.value,
                                      variant: bar.variant
                                    }
                                  })
                                ],
                                1
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code5.html) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v(
                        "\n          " + _vm._s(_vm.code5.js) + "\n        "
                      )
                    ]
                  },
                  proxy: true
                }
              ])
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Progress.vue?vue&type=template&id=6845cd91& */ "./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91&");
/* harmony import */ var _Progress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Progress.vue?vue&type=script&lang=js& */ "./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Progress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Progress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Progress.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Progress_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Progress.vue?vue&type=template&id=6845cd91& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Progress.vue?vue&type=template&id=6845cd91&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Progress_vue_vue_type_template_id_6845cd91___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);