<?php

use Illuminate\Contracts\Validation\Rule;

class IsEmailValid implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match('/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u', $value);
    }
    public function message()
    {
        return 'Введен некорректный адрес Email';
    }
}
