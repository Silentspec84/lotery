<?php

namespace App\Http\Controllers;

use App\Helpers\Cpayeer;
use App\Models\Settings;
use Illuminate\Http\Request;

class PayController extends Controller
{
    private const ACC_NUMBER = 'P1042716222';
    private const API_ID = '1288091584';
    private const API_KEY = 'tn5xDv8VgaHmBOSU';

    private $payeer;

    public function getPayData(Request $request)
    {
        $raw_pay_data = $this->getPaySystems();
        ea($raw_pay_data);
        $pay_settings = [];
        foreach ($raw_pay_data['list'] as $pay_system) {
            if ($pay_system['id'] === '1136053') {
                $pay_settings[] = [
                    'id' => $pay_system['id'],
                    'name' => $pay_system['name'],
                    'account_ex' => $pay_system['r_fields']['ACCOUNT_NUMBER']['example'],
                    'account_reg_exp' => str_replace('#', '', $pay_system['r_fields']['ACCOUNT_NUMBER']['reg_expr']),
                    'commission' => $pay_system['commission_site_percent']
                ];
            }
            if ($pay_system['id'] === '22238179') {
                $pay_settings[] = [
                    'id' => $pay_system['id'],
                    'name' => $pay_system['name'],
                    'account_ex' => $pay_system['r_fields']['ACCOUNT_NUMBER']['example'],
                    'account_reg_exp' => str_replace('#', '', $pay_system['r_fields']['ACCOUNT_NUMBER']['reg_expr']),
                    'commission' => 0.99
                ];
            }
            if ($pay_system['id'] === '87893285') {
                $pay_settings[] = [
                    'id' => $pay_system['id'],
                    'name' => $pay_system['name'],
                    'account_ex' => $pay_system['r_fields']['ACCOUNT_NUMBER']['example'],
                    'account_reg_exp' => str_replace('#', '', $pay_system['r_fields']['ACCOUNT_NUMBER']['reg_expr']),
                    'commission' => 1.99
                ];
            }
        }

        return response()->json(['success' => true, 'pay_settings' => $pay_settings]);
    }

    public function withwraw(Request $request)
    {
        return response()->json(['success' => true]);
    }

    private function getPayeer()
    {
        if (!$this->payeer) {
            $this->payeer = new Cpayeer(PayController::ACC_NUMBER, PayController::API_ID, PayController::API_KEY);
        }
        return $this->payeer;
    }

    /**
     * [ auth_error => 0, balance => [
     * BCH => [BUDGET => 0, DOSTUPNO => 0, DOSTUPNO_SYST => 0], BTC, DAA, ETH, EUR, LTC, RUB, USD, UST, XRP
     * ], errors => []
     * @return mixed
     */
    private function getBalance()
    {
        $this->getPayeer();
        if ($this->payeer->isAuth()) {
            return $this->payeer->getBalance();
        } else {
            return $this->payeer->getErrors();
        }
    }

    /**
     * [auth_error => 0, errors => [],
     * list => [
     *      1136053 => [ id => 1136053, name => Payeer, commission_site_percent => 0.95, currencies => [0 => USD, 1 => RUB, ...],
     *              gate_commission => [], gate_commission_max => [], gate_commission_min => [],
     *              r_fields => [ACCOUNT_NUMBER=>[example=>P1000000, name=>Номер счета, reg_expr=> "#^[Pp]{1}[0-9]{7,15}|.+@.+\..+$#"]],
     *              sum_max => [BCH => 1000, USD => 10000, ...],
     *              sum_min => [BCH => 0.001, EUR => 0.02, RUB => 1]
     * ]
     *
     * @return mixed
     */
    private function getPaySystems()
    {
        $this->getPayeer();
        if ($this->payeer->isAuth()) {
            return $this->payeer->getPaySystems();
        } else {
            return $this->payeer->getErrors();
        }
    }

}
