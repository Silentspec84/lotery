<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Settings
 *
 * @property string $name
 * @property string $value
 *
 * @package App
 */
class Settings extends Model
{
    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'value'
    ];

    public static function getValueByName(string $name) {
        return self::where('name', $name)->first();
    }
}
