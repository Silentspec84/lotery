import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import htmlClass from "./htmlclass.module";
import config from "./config.module";
import breadcrumbs from "./breadcrumbs.module";
import profile from "./profile.module";
import user from "./user.module";
import stats from "./stats.module";
import advs from "./advs.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        htmlClass,
        config,
        breadcrumbs,
        profile,
        user,
        stats,
        advs
    },
    plugins: [createPersistedState()],
});
