/**
 * Mixin рекламы
 */

export const advMixins = {
    data() {
        return {
            userDeviceArray: [
                {device: 'Android', platform: /Android/},
                {device: 'iPhone', platform: /iPhone/},
                {device: 'iPad', platform: /iPad/},
                {device: 'Symbian', platform: /Symbian/},
                {device: 'Windows Phone', platform: /Windows Phone/},
                {device: 'Tablet OS', platform: /Tablet OS/},
                {device: 'Linux', platform: /Linux/},
                {device: 'Windows', platform: /Windows NT/},
                {device: 'Macintosh', platform: /Macintosh/}
            ],
            platform: navigator.userAgent
        };
    },
    methods: {
        goadv: function() {
            var link = '';
            switch(this.getPlatform()) {
                case 'Android':
                    link = this.isAdaptive() ? 'https://suhba.world' : 'https://play.google.com/store/apps/details?id=com.suhba&hl=en_US';
                    break;
                case 'iPhone':
                    link = this.isAdaptive() ? 'https://suhba.world' : 'https://apps.apple.com/ru/app/suhba-network/id1488683504';
                    break;
                default:
                    link = this.isAdaptive() ? 'https://suhba.world' : 'https://suhba.net/';
                    break;
            }
            window.open(link, '_blank');
        },
        getPlatform: function () {
            for (var i in userDeviceArray) {
                if (userDeviceArray[i].platform.test(platform)) {
                    return userDeviceArray[i].device;
                }
            }
            return 'Неизвестная платформа!' + platform;
        },
        isAdaptive: function () {
            return $(document.body).hasClass('mobile');
        },
    },
};
