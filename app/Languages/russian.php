<?php
// Language Name
$name = 'Русский';

// Language Author
$author = 'sba.plus';

// Language URL
$url = 'https://sba.plus';

// Language Direction
$lang['lang_dir'] = 'ltr';

// Menu
$lang['search'] = 'Поиск';
$lang['web'] = 'Веб';
$lang['images'] = 'Картинки';
$lang['videos'] = 'Видео';
$lang['news'] = 'Новости';

$lang['info'] = 'Инфо';
$lang['themes'] = 'Темы';
$lang['language'] = 'Язык';
$lang['languages'] = 'Языки';

// Admin Panel
$lang['admin'] = 'Админ';
$lang['general'] = 'Основные';
$lang['appearance'] = 'Внешний вид';
$lang['info_pages'] = 'Инфо страницы';
$lang['username'] = 'Имя пользователя';
$lang['password'] = 'Пароль';
$lang['login'] = 'Вход';
$lang['logout'] = 'Выход';
$lang['save'] = 'Сохранить';
$lang['remember_me'] = 'Запомнить меня';
$lang['recover'] = 'Восстановить';
$lang['by'] = 'By';
$lang['active'] = 'Активно';
$lang['activate'] = 'Активировать';
$lang['default'] = 'Язык по умолчанию';
$lang['make_default'] = 'Сделать языком по умолчанию';
$lang['public'] = 'Публичные';
$lang['unlisted'] = 'Не в списке';
$lang['edit'] = 'Редактировать';
$lang['new_page'] = 'Новая страница';
$lang['delete'] = 'Удалить';
$lang['cancel'] = 'Отмена';
$lang['yes'] = 'Да';
$lang['no'] = 'Нет';
$lang['off'] = 'Выключено';
$lang['on'] = 'Включено';
$lang['change'] = 'Изменить';
$lang['validate_license'] = 'Пожалуйста, подтвердите свой лицензионный ключ (код покупки), прежде чем продолжить.';
$lang['cookie_policy_url'] = 'URL политики использования файлов cookie';
$lang['cookie_text'] = 'Мы используем файлы cookie, чтобы вам было удобнее пользоваться нашим сайтом. Просматривая этот сайт, вы соглашаетесь на использование файлов cookie.';
$lang['ok'] = 'OK';
$lang['more_info'] = 'Больше информации';

$lang['dashboard'] = 'Панель управления';
$lang['site_title'] = 'Название сайта';
$lang['site_tagline'] = 'Слоган';
$lang['logo_small'] = 'Логотип маленький';
$lang['logo_large'] = 'Логотип большой';
$lang['favicon'] = 'Фавикон';
$lang['backgrounds'] = 'Фоны';
$lang['timezone'] = 'Часовой пояс';
$lang['tracking_code'] = 'Код отслеживания';
$lang['current_password'] = 'Текущий пароль';
$lang['new_password'] = 'Новый пароль';
$lang['confirm_new_password'] = 'Подтвердите новый пароль';

$lang['settings_saved'] = 'Настройки сохранены';

$lang['upload_error_code'] = 'Код ошибки загрузки: %s';
$lang['invalid_user_pass'] = 'Неправильное имя пользователя или пароль';
$lang['wrong_current_password'] = 'Текущий пароль, который вы ввели, недействителен.';
$lang['password_too_short'] = 'Пароль слишком короткий';
$lang['password_not_matching'] = 'Пароль не совпадает';
$lang['all_fields_required'] = 'Все поля обязательны к заполнению';

$lang['page_title'] = 'Заголовок страницы';
$lang['page_url'] = 'URL страницы';
$lang['page_public'] = 'Публичная страница';
$lang['page_content'] = 'Содержание страницы';
$lang['page_url_exists'] = 'URL страницы уже существует';
$lang['page_created'] = 'Страница %s создана';
$lang['page_deleted'] = 'Страница %s удалена';

$lang['site_info'] = 'Информация о сайте';
$lang['useful_links'] = 'Полезные ссылки';
$lang['get_more_themes'] = 'Получить больше тем';
$lang['get_more_languages'] = 'Получить больше языков';

$lang['search_api_key'] = 'Ключ API';
$lang['web_per_page'] = 'Веб результатов на страницу';
$lang['images_per_page'] = 'Результатов с картинками на страницу';
$lang['videos_per_page'] = 'Результатов с видео на страницу';
$lang['news_per_page'] = 'Результатов с новостями на страницу';
$lang['searches_per_ip'] = 'Поисковых запросов на один IP';
$lang['search_suggestions'] = 'Предложения по поиску';
$lang['search_entities'] = 'Искать объекты';
$lang['search_answers'] = 'Искать ответы';
$lang['suggestions_per_ip'] = 'Suggestions / IP';
$lang['search_specific_sites'] = 'Искать только на определенных сайтах';
$lang['search_privacy'] = 'Конфиденциальность поиска';
$lang['one_domain_line'] = 'Один домен на строку';

$lang['ads'] = 'Реклама';
$lang['ads_safe'] = 'Безопасная реклама';
$lang['ads_1'] = 'Домашняя страница';
$lang['ads_2'] = 'Страница поиска (Верхнее меню)';
$lang['ads_3'] = 'Страница поиска (Подвал)';
$lang['ad_unit_code'] = 'Код рекламного блока';

// Home
$lang['preferences'] = 'Настройки';

// Настройки
$lang['theme'] = 'Тема';
$lang['region'] = 'Регион';
$lang['dark_mode'] = 'Ночной режим';
$lang['center_content'] = 'Центрировать контент';
$lang['site_language'] = 'Язык сайта';
$lang['new_window'] = 'Открывать результаты в новой вкладке';
$lang['highlight'] = 'Подсвечивать ключевые слова в результатах поиска';

// Misc
$lang['token_mismatch'] = 'Несоответствие токенов';
$lang['copyright'] = '&copy; %s %s';
$lang['powered_by'] = 'Собственность %s';

// Search
$lang['filters'] = 'Фильтры';
$lang['period'] = 'Период';
$lang['past_day'] = 'За прошлый день';
$lang['past_week'] = 'За прошлую неделю';
$lang['past_month'] = 'За прошлый месяц';
$lang['past_year'] = 'За прошлый год';

$lang['type'] = 'Тип';
$lang['duration'] = 'Длительность';
$lang['quality'] = 'Качество';
$lang['all'] = 'Все';

$lang['safe_search'] = 'Безопасный поиск';
$lang['moderate'] = 'Умеренный';
$lang['strict'] = 'Строгий';
$lang['short'] = 'Короткие';
$lang['medium'] = 'Средние';
$lang['long'] = 'Длинные';

$lang['aspect'] = 'Форма';
$lang['animated'] = 'Анимированные';
$lang['clipart'] = 'Клипарт';
$lang['line'] = 'Линейные';
$lang['photo'] = 'Фото';
$lang['transparent'] = 'Прозрачные';
$lang['square'] = 'Квадратные';
$lang['wide'] = 'Широкие';
$lang['tall'] = 'Вытянутые';
$lang['size'] = 'Размер';
$lang['small'] = 'Маленькие';
$lang['large'] = 'Большие';
$lang['wallpaper'] = 'Обои';
$lang['color'] = 'Цвета';
$lang['colored'] = 'Цветные';
$lang['monochrome'] = 'Монохром';
$lang['license'] = 'Лицензия';
$lang['share'] = 'Бесплатно для личного пользования';
$lang['share_commercially'] = 'Бесплатно для использования в коммерческих целях';
$lang['modify'] = 'Бесплатно для личного изменения';
$lang['modify_commercially'] = 'Бесплатно для изменения в коммерческих целях';

$lang['sort_by'] = 'Сортировка';
$lang['relevance'] = 'Актуальность';
$lang['date'] = 'Дата';

$lang['source'] = 'Источник';
$lang['image'] = 'Картинка';

$lang['search_l_e'] = 'Вы превысили дневной лимит поиска. Повторите попытку завтра.';
$lang['no_results_found'] = 'Ничего не найдено для запроса %s.';
$lang['suggestions'] = 'Предложения:';
$lang['suggestion_1'] = 'Попробуйте менее конкретные ключевые слова.';
$lang['suggestion_2'] = 'Попробуйте другие ключевые слова.';
$lang['suggestion_3'] = 'Попробуйте меньше ключевых слов.';

$lang['showing_results_for'] = 'Показываем результаты для %s';
$lang['search_instead_for'] = 'Искать вместо этого %s';
$lang['related_searches'] = 'Похожие запросы';
$lang['more_x'] = 'Больше %s';
$lang['x_results'] = '%s результатов';

$lang['data_from'] = 'Данные из:';
$lang['text_under'] = 'Текст под %s';
$lang['read_more'] = 'Читать больше';
$lang['see_results_for'] = 'Смотреть результаты для';

$lang['date_format'] = '%2$s %3$s, %1$s';

$lang['views_x'] = '%s просмотров';
$lang['views_k'] = 'K';
$lang['views_m'] = 'M';
$lang['views_b'] = 'B';
$lang['views_t'] = 'T';
$lang['month_01'] = 'Январь';
$lang['month_02'] = 'Февраль';
$lang['month_03'] = 'Март';
$lang['month_04'] = 'Апрель';
$lang['month_05'] = 'Май';
$lang['month_06'] = 'Июнь';
$lang['month_07'] = 'Июль';
$lang['month_08'] = 'Август';
$lang['month_09'] = 'Сентябрь';
$lang['month_10'] = 'Октябрь';
$lang['month_11'] = 'Ноябрь';
$lang['month_12'] = 'Декабрь';

$lang['decimals_separator'] = '.';
$lang['thousands_separator'] = ',';
$lang['ellipsis'] = '...';

// Instant Answers triggers
$lang['ia']['ip'][] = 'какой у меня ip';
$lang['ia']['ip'][] = 'мой ip';
$lang['ia']['ip'][] = 'ip';
$lang['ia']['time'][] = 'текущее время';
$lang['ia']['time'][] = 'локальное время';
$lang['ia']['time'][] = 'время';
$lang['ia']['date'][] = 'текущая дата';
$lang['ia']['date'][] = 'локальная дата';
$lang['ia']['date'][] = 'дата';
$lang['ia']['flip_coin'][] = 'подбросить монету';
$lang['ia']['flip_coin'][] = 'Орел или решка';
$lang['ia']['stopwatch'][] = 'секундомер';
$lang['ia']['stopwatch'][] = 'хронометр';
$lang['ia']['roll'][] = 'вращать';
$lang['ia']['qr_code'][] = 'qr';
$lang['ia']['qr_code'][] = 'qr code';
$lang['ia']['sort_asc'][] = 'сортировка';
$lang['ia']['sort_asc'][] = 'Сортировать по возрастанию';
$lang['ia']['sort_desc'][] = 'Сортировать по убыванию';
$lang['ia']['reverse_text'][] = 'перевернуть текст';
$lang['ia']['md5'][] = 'md5';
$lang['ia']['base64_encode'][] = 'base64 encode';
$lang['ia']['base64_decode'][] = 'base64 decode';
$lang['ia']['lowercase'][] = 'нижний регистр';
$lang['ia']['uppercase'][] = 'верхний регистр';
$lang['ia']['camelcase'][] = 'camelcase';
$lang['ia']['uuid'][] = 'uuid';
$lang['ia']['uuid'][] = 'guid';
$lang['ia']['leap_year'][] = 'високосный год';
$lang['ia']['screen_resolution'][] = 'разрешение экрана';
$lang['ia']['pi'][] = 'pi';
$lang['ia']['pi'][] = 'π';
$lang['ia']['morse_code'][] = 'азбука Морзе';
$lang['ia']['unix_time'][] = 'время unix';
$lang['ia']['lorem_ipsum'][] = 'lorem ipsum';
$lang['ia']['atbash'][] = 'atbash';

// Cards
$lang['your_ip_is'] = 'Ваш IP';
$lang['current_time_is'] = 'Текущее время';
$lang['current_date_is'] = 'Текущая дата';
$lang['you_have_flipped'] = 'Вы перевернули';
$lang['coin_0'] = 'Головы';
$lang['coin_1'] = 'Хвосты';
$lang['stopwatch'] = 'Секундомер';
$lang['start'] = 'Начало';
$lang['stop'] = 'Стоп';
$lang['reset'] = 'Сброс';
$lang['you_have_rolled'] = 'You have rolled';
$lang['qr_code_for'] = 'QR code for %s';
$lang['sorted_1'] = 'Sorted in ascending order';
$lang['sorted_2'] = 'Sorted in descending order';
$lang['reversed_text'] = 'Reversed text is';
$lang['md5_hash_for'] = 'MD5 hash for %s';
$lang['base64_1'] = 'Base64 encoding for %s';
$lang['base64_2'] = 'Base64 decoding for %s';
$lang['case_1'] = 'Lowercase';
$lang['case_2'] = 'Uppercase';
$lang['case_3'] = 'Camelcase';
$lang['random_uuid'] = 'Random UUID';
$lang['leap_year_1'] = 'Is a leap year';
$lang['leap_year_2'] = 'Is not a leap year';
$lang['your_sr_is'] = 'Your screen resolution is';
$lang['pi_is'] = 'The Pi value is';
$lang['morse_code_for'] = 'The morse code for %s';
$lang['unix_time'] = 'Unix time';
$lang['unix_time_for'] = 'Date for %s Unix Time';
$lang['gmt'] = 'GMT';
$lang['x_lorem_ipsum'] = '%s paragraphs of Lorem Ipsum';
$lang['atbash_for'] = 'Atbash cipher for %s';
