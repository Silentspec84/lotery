(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "./frontend/views/admin/index.vue":
/*!****************************************!*\
  !*** ./frontend/views/admin/index.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=15855f42&scoped=true& */ "./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./frontend/views/admin/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "15855f42",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/views/admin/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./frontend/views/admin/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./frontend/views/admin/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/admin/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true&":
/*!***********************************************************************************!*\
  !*** ./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=15855f42&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_15855f42_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/admin/index.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/admin/index.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "index",
  data: function data() {
    return {
      player: {
        status: false,
        value: ''
      },
      winner: {
        status: false,
        value: ''
      }
    };
  },
  methods: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapActions"])('user', ['updateUser'])), {}, {
    runPlayer: function runPlayer() {
      var _this = this;

      axios.post('/api/run-player').then(function (response) {
        if (!response.data.success) {} else {
          _this.getPlayer();
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    stopPlayer: function stopPlayer() {
      var _this2 = this;

      axios.post('/api/stop-player').then(function (response) {
        if (!response.data.success) {} else {
          _this2.getPlayer();
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    getPlayer: function getPlayer() {
      var _this3 = this;

      axios.post('/api/get-player').then(function (response) {
        if (!response.data.success) {} else {
          _this3.player = response.data.player;
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    runWinner: function runWinner() {
      var _this4 = this;

      axios.post('/api/run-winner').then(function (response) {
        if (!response.data.success) {} else {
          _this4.getWinner();
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    stopWinner: function stopWinner() {
      var _this5 = this;

      axios.post('/api/stop-winner').then(function (response) {
        if (!response.data.success) {} else {
          _this5.getPlayer();
        }
      })["catch"](function (err) {
        console.log(err);
      });
    },
    getWinner: function getWinner() {
      var _this6 = this;

      axios.post('/api/get-winner').then(function (response) {
        if (!response.data.success) {} else {
          _this6.winner = response.data.winner;
        }
      })["catch"](function (err) {
        console.log(err);
      });
    }
  }),
  mounted: function mounted() {
    this.getPlayer();
    this.getWinner();

    if (this.user.role !== 'admin') {
      this.$router.push('/');
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])('user', ['user']))
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./frontend/views/admin/index.vue?vue&type=template&id=15855f42&scoped=true& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "b-container",
    { staticClass: "h-100 mt-6 mb-4", attrs: { fluid: "" } },
    [
      _c(
        "b-row",
        [
          _c("b-col", { staticClass: "mt-4 col-l", attrs: { cols: "2" } }),
          _vm._v(" "),
          _c(
            "b-col",
            { staticClass: "mt-4", attrs: { cols: "8" } },
            [
              _c(
                "b-container",
                { attrs: { fluid: "" } },
                [
                  _c(
                    "b-card",
                    {
                      attrs: {
                        header: "Управление",
                        "header-tag": "h1",
                        "header-bg-variant": "dark",
                        "header-class": "card_header"
                      }
                    },
                    [
                      _c(
                        "b-card-text",
                        [
                          _c("h1", [_vm._v("Плеер")]),
                          _vm._v(" "),
                          _vm.player && !_vm.player.status
                            ? _c("b-button", { on: { click: _vm.runPlayer } }, [
                                _vm._v("Включить плеер")
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.player && _vm.player.status
                            ? _c(
                                "b-button",
                                { on: { click: _vm.stopPlayer } },
                                [_vm._v("Выключить плеер")]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c("span", [_vm._v(_vm._s(_vm.player.value))]),
                          _vm._v(" "),
                          _c("h1", [_vm._v("Розыгрыш")]),
                          _vm._v(" "),
                          _vm.winner && !_vm.winner.status
                            ? _c("b-button", { on: { click: _vm.runWinner } }, [
                                _vm._v("Включить виннер")
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.winner && _vm.winner.status
                            ? _c(
                                "b-button",
                                { on: { click: _vm.stopWinner } },
                                [_vm._v("Выключить виннер")]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c("span", [_vm._v(_vm._s(_vm.winner.value))])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("b-col", { staticClass: "mt-4 col-r", attrs: { cols: "2" } })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);