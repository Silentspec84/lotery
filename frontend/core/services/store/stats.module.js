export default {
    namespaced: true,
    state: {
        stats: null,
        yesturday_stats: null,
    },
    getters: {
        stats(state) {
            return state.stats;
        },
        yesturday_stats(state) {
            return state.yesturday_stats;
        }
    },
    mutations: {
        setStats(state, stats) {
            state.stats = stats;
        },
        setYesturdayStats(state, yesturday_stats) {
            state.yesturday_stats = yesturday_stats;
        },
    },
    actions: {
        updateStats(context, payload) {
            context.commit('setStats', payload);
        },
        updateYesturdayStats(context, payload) {
            context.commit('setYesturdayStats', payload);
        },
    },
};
