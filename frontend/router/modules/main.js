import UserLayout from '@/layouts/user';

const mainRoutes = {
  path: '',
  component: UserLayout,
  children: [
      {
          path: '',
          name: 'main',
          component: () => import('@/views/landing/index'),
      },
      {
          path: '/rules',
          name: 'rules',
          component: () => import('@/views/rules/index'),
      },
      {
          path: '/agreement',
          name: 'agreement',
          component: () => import('@/views/agreement/index'),
      },
      {
          path: '/settings',
          name: 'settings',
          component: () => import('@/views/settings/index'),
      },
      {
          path: '/get-lottery',
          name: 'get-lottery',
          component: () => import('@/views/get-lottery/index'),
      },
      {
          path: '/check-lottery',
          name: 'check-lottery',
          component: () => import('@/views/check-lottery/index'),
      },
      {
          path: '/login',
          name: 'login',
          component: () => import('@/views/login/index'),
      },
      {
          path: '/register',
          name: 'register',
          component: () => import('@/views/register/index'),
      },
      {
          path: '/admin',
          name: 'admin',
          component: () => import('@/views/admin/index'),
      },
  ],
};
export default mainRoutes;
