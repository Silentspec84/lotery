// USA
export const locale = {
  TRANSLATOR: {
    SELECT: "Select your language"
  },
  MENU: {
    NEW: "new",
    ACTIONS: "Actions",
    CREATE_POST: "Create New Post",
    PAGES: "Pages",
    FEATURES: "Features",
    APPS: "Apps",
    DASHBOARD: "Dashboard"
  },
  AUTH: {
    GENERAL: {
      OR: "Or",
      SUBMIT_BUTTON: "Submit",
      NO_ACCOUNT: "Don't have an account?",
      SIGNUP_BUTTON: "Sign Up",
      FORGOT_BUTTON: "Forgot Password",
      BACK_BUTTON: "Back",
      PRIVACY: "Privacy",
      LEGAL: "Legal",
      CONTACT: "Contact"
    },
    LOGIN: {
      TITLE: "Login Account",
      BUTTON: "Sign In"
    },
    FORGOT: {
      TITLE: "Forgot Password?",
      DESC: "Enter your email to reset your password",
      SUCCESS: "Your account has been successfully reset."
    },
    REGISTER: {
      TITLE: "Sign Up",
      DESC: "Enter your details to create your account",
      SUCCESS: "Your account has been successfuly registered."
    },
    INPUT: {
      EMAIL: "Email",
      FULLNAME: "Fullname",
      PASSWORD: "Password",
      CONFIRM_PASSWORD: "Confirm Password",
      USERNAME: "Username"
    },
    VALIDATION: {
      INVALID: "{{name}} is not valid",
      REQUIRED: "{{name}} is required",
      MIN_LENGTH: "{{name}} minimum length is {{min}}",
      AGREEMENT_REQUIRED: "Accepting terms & conditions are required",
      NOT_FOUND: "The requested {{name}} is not found",
      INVALID_LOGIN: "The login detail is incorrect",
      REQUIRED_FIELD: "Required field",
      MIN_LENGTH_FIELD: "Minimum field length:",
      MAX_LENGTH_FIELD: "Maximum field length:",
      INVALID_FIELD: "Field is not valid"
    }
  },
  ECOMMERCE: {
    COMMON: {
      SELECTED_RECORDS_COUNT: "Selected records count: ",
      ALL: "All",
      SUSPENDED: "Suspended",
      ACTIVE: "Active",
      FILTER: "Filter",
      BY_STATUS: "by Status",
      BY_TYPE: "by Type",
      BUSINESS: "Business",
      INDIVIDUAL: "Individual",
      SEARCH: "Search",
      IN_ALL_FIELDS: "in all fields"
    },
    ECOMMERCE: "eCommerce",
    CUSTOMERS: {
      CUSTOMERS: "Customers",
      CUSTOMERS_LIST: "Customers list",
      NEW_CUSTOMER: "New Customer",
      DELETE_CUSTOMER_SIMPLE: {
        TITLE: "Customer Delete",
        DESCRIPTION: "Are you sure to permanently delete this customer?",
        WAIT_DESCRIPTION: "Customer is deleting...",
        MESSAGE: "Customer has been deleted"
      },
      DELETE_CUSTOMER_MULTY: {
        TITLE: "Customers Delete",
        DESCRIPTION: "Are you sure to permanently delete selected customers?",
        WAIT_DESCRIPTION: "Customers are deleting...",
        MESSAGE: "Selected customers have been deleted"
      },
      UPDATE_STATUS: {
        TITLE: "Status has been updated for selected customers",
        MESSAGE: "Selected customers status have successfully been updated"
      },
      EDIT: {
        UPDATE_MESSAGE: "Customer has been updated",
        ADD_MESSAGE: "Customer has been created"
      }
    }
  },
    search: {
        filters: {
            freshness: {
                title: 'Period',
                all: 'All',
                past_day: 'For the last day',
                past_week: 'For the last week',
                past_month: 'For the last month',
            },
            fileType: {
                title: 'File type',
                all: 'All',
                doc: 'doc',
                docx: 'docx',
                dwf: 'dwf',
                pdf: 'pdf',
                ppt: 'ppt',
                pptx: 'pptx',
                psd: 'psd',
                xls: 'xls',
                xlsx: 'xlsx',
            },
            safeSearch: {
                title: 'Safe Search',
                off: 'Off',
                moderate: 'Moderate',
                strict: 'Strict'
            },
            aspect: {
                title: 'Form',
                all: 'All',
                square: 'Square',
                wide: 'Wide',
                tall: 'Tall'
            },
            color: {
                title: 'Color',
                all: 'All',
                colored: 'Colored',
                monochrome: 'Monochrome'
            },
            size: {
                title: 'Size',
                all: 'All',
                small: 'Small',
                medium: 'Medium',
                large: 'Large',
                wallpaper: 'Wallpaper',
            },
            imageType: {
                title: 'Image type',
                all: 'All',
                animated: 'Animated',
                clipart: 'Clipart',
                line: 'Line',
                photo: 'Photo',
                transparent: 'Transparent'
            },
            license: {
                title: 'License',
                all: 'All',
                public: 'Public',
                share: 'Share',
                share_commercially: 'Share commercially',
                modify: 'Modify',
                modify_commercially: 'Modify commercially',
            },
            videoLength: {
                title: 'Length',
                all: 'All',
                short: 'Short',
                medium: 'Medium',
                long: 'Long',
            },
            resolution: {
                title: 'screen resolution',
                all: 'All',
                '480p': '480p',
                '720p': '720p',
                '1080p': '1080p',
            },
            sortBy: {
                title: 'Sort by',
                relevance: 'Relevance',
                date: 'Date'
            }
        }
    },
    main: {
        search_title: 'Search',
        search_placeholder: 'What are we looking for?',
    },
    web_page: {
        filters: 'Filters',
        results_count: 'Pages found matching your request:',
        related_searches: 'Related searches:',
        videos: 'Videos',
        web: 'Web pages',
        images: 'Images',
        news: 'News',
        show_videos: 'Show more videos',
        hide_videos: 'Hide videos',
        show_images: 'Show more images',
        hide_images: 'Hide images',
        search: 'Search'
    },
    message: {
        empty_query: 'You sent an empty request!',
        empty_result: 'Sorry, no results were found! Try to refine your request.',
        search_limit: 'Sorry, you have reached the search limit for today! Come back tomorrow.'
    },
    navbar: {
        settings: 'SETTINGS',
        messenger: 'MESSENGER',
        mail: 'POCHTA',
        disk: 'DISK',
    },
    services: {
        all: 'ALL SERVICES SBA+',
        all_desc: 'At the moment, some of the services are under development and are not available for use',
        all_services: 'ALL SERVICES',
        all_apps: 'ALL APPS',
        all_programs: 'ALL PROGRAMS',
        all_biz: 'FOR BUSINESS',
        all_services_sba: 'SBA+ APPLICATIONS',
        all_services_title: 'This section contains all applications of the project SBA+',
        app_video: 'VIDEO+',
        app_video_descr: 'Download the VIDEO + app. Video Blog is a hosting for hosting, storing, searching, renting and selling video files and films.',
        app_sba: 'SBA+',
        app_sba_descr: 'Download the SBA PLUS app. SBA + is a Super App application where you can use all project services through one application on your phone.',
        app_online: 'ONLINE+',
        app_online_descr: 'Download the ONLINE + app. Next generation business trading platform based on the principles of social communities on the Internet.',
        app_photogram: 'PHOTOGRAM+',
        app_photogram_descr: 'Download the PHOTOGRAM app. A service that allows you to easily and conveniently shoot creative photos and videos, edit them, and also share them.',
        app_connect: 'CONNECT+',
        app_connect_descr: 'Download the CONNECT messenger. Connect is a website, program and application for exchanging text messages, making audio and video calls.',
        app_mail: 'POCHTA+',
        app_mail_descr: 'Download the POCHTA + mail service. MAIL PLUS is a secure email hosting. Antivirus, antispam, SSL encryption of all protocols and no ads.',
        app_shortlinks: 'SHORTLINKS+',
        app_shortlinks_descr: 'Download the SHORTLINKS + app. Link shortening services help shorten long urls for use on social media.',
        app_constr: 'VVEB+',
        app_constr_descr: 'Download VVEB + website builder. Create a free website, landing page or online store using the website builder in just a couple of hours.',
        app_help: 'HELPDESK+',
        app_help_descr: 'Download the HELPDESK + app. Support Center is an information system for technical support, solving user problems.',
        app_disk: 'DISK+',
        app_disk_descr: 'Download the DISK + app. Disk is a service that allows you to store and transfer files to any device connected to the Internet.',
        app_store: 'STORE+',
        app_store_descr: 'Download the STORE + app. Store plus is a proprietary Android app store with easy navigation and intuitive interface.',

        biz_title: 'BUSINESS SERVICES SBA+',
        biz_descr: 'Services that either help promote your business or are themselves a source of profit',
        biz_platform: 'Business platform',
        biz_platform_descr: 'Next generation business trading platform based on the principles of social communities on the Internet',
        biz_video: 'Video blog',
        biz_video_descr: 'Video blog is a hosting for hosting, storing, searching, renting and selling video files',
        biz_aladdin: 'Aladdin Market',
        biz_aladdin_descr: 'This is an electronic multi-vendor multi-currency multilingual marketplace',
        biz_market: 'App Store',
        biz_market_descr: 'Own Android application store with easy navigation and intuitive interface',
        biz_mail: 'Post service',
        biz_mail_descr: 'Protected email hosting. Antivirus, antispam, SSL encryption of all protocols and no ads',
        biz_messenger: 'Messenger Connect',
        biz_messenger_descr: 'Website application to exchange text messages, make audio and video calls',
        biz_shortlinks: 'Short link service',
        biz_shortlinks_descr: 'Link shortening services help shorten long urls for use on social media',
        biz_disk: 'Storage service',
        biz_disk_descr: 'A service that allows you to store and transfer files to any device connected to the Internet',
        biz_webinar: 'Webinar room',
        biz_webinar_descr: 'Works in accordance with world security standards, all data is copied and backed up',
        biz_site_constr: 'Website builder',
        biz_site_constr_descr: 'Create a free website, landing page or online store using the website builder in just a couple of hours',
        biz_adv: 'Advertising on the project',
        biz_adv_descr: 'Contextual and display advertising will help increase sales and attract new customers for your business',
        biz_online: 'Online learning',
        biz_online_descr: 'Online educational platform to increase personal brand awareness and increase income',
        biz_craud: 'Crowdfunding',
        biz_craud_descr: 'A crowdfunding platform is a platform used to host and promote projects on the Internet',
        biz_stock: 'SBA INVEST STOCK',
        biz_stock_descr: 'A platform with a P2P exchange service for Smart Shares of the Company SBA INVEST',
        biz_aladdin_stock: 'Aladdin STOCK',
        biz_aladdin_stock_descr: 'Platform with P2P Exchange service for Smart Shares of the Aladdin Company',
        biz_coin: 'SBA COIN STOCK',
        biz_coin_descr: 'Platform with P2P Exchanges service for SBA COIN company SBA INVEST',
        biz_service_started: 'Service started',
        biz_service_starting: 'Coming soon',
        biz_service_finish: 'Service suspended',

        prog_title: 'SBA+ PROGRAMS',
        prog_descr: 'This section contains all programs for downloading and using on Windows',
        prog_progress: 'In progress',
        prog_progress_descr: 'This page is under construction',

        service_title: 'Business platform',
        service_descr: 'Next generation business trading platform based on the principles of social communities on the Internet',
        service_video: 'Video blog',
        service_video_descr: 'Video blog is a hosting for hosting, storing, searching, renting and selling video files',
        service_market: 'Aladdin Market',
        service_market_descr: 'This is an electronic multi-vendor multi-currency multilingual marketplace',
        service_app_shop: 'App Store',
        service_app_shop_descr: 'Own Android application store with easy navigation and intuitive interface',
        service_mail: 'Post service',
        service_mail_descr: 'Protected email hosting. Antivirus, antispam, SSL encryption of all protocols and no ads',
        service_disk: 'Storage service',
        service_disk_descr: 'A service that allows you to store and transfer files to any device connected to the Internet',
        service_messegger: 'Messenger Connect',
        service_messegger_descr: 'Website, program and application for text, audio and video messaging',
        service_link: 'Short link service',
        service_link_descr: 'Link shortening services help shorten long urls for use on social media',
        service_soc_net: 'Social network',
        service_soc_net_descr: 'A platform, online service or website for building and organizing social relationships',
        service_webinar: 'Webinar room',
        service_webinar_descr: 'Works in accordance with world security standards, all data is copied and backed up',
        service_site_constr: 'Website builder',
        service_site_constr_descr: 'Create a free website, landing page or online store using the website builder in just a couple of hours',
        service_photo: 'PHOTOGRAM',
        service_photo_descr: 'A service that allows you to easily and conveniently shoot creative photos and videos, edit them, and also share them',
        service_star_map: 'Star map',
        service_star_map_descr: 'An image of the starry sky or part of it, showing objects of stars, planets, comets located on it',
        service_adv: 'Advertising on the project',
        service_adv_descr: 'Contextual and display advertising will help increase sales and attract new customers for your business',
        service_search: 'Search service',
        service_search_descr: 'The system of algorithms realizing the search for the information you need on the Internet',
        service_teach: 'Online learning',
        service_teach_descr: 'Online educational platform to increase personal brand awareness and increase income',
        service_craud: 'Crowdfunding',
        service_craud_descr: 'A crowdfunding platform is a platform used to host and promote projects on the Internet',
        service_support: 'Support service',
        service_support_descr: 'Technical support through which businesses and organizations provide assistance to users',
        service_started: 'Service started',
        service_starting: 'Coming soon',
        service_finish: 'Service suspended',
    },
    box_menu: {
        business_br: 'Business<br>online',
        business: 'Business online',
        app_shop_br: 'App<br>shop',
        app_shop: 'App shop',
        star_map_br: 'Star<br>map',
        star_map: 'Star map',
        photogram_br: 'Photo<br>gram',
        photogram: 'Photogram',
        video_br: 'Video<br>blog',
        video: 'Video blog',
        webinar_br: 'Webinar<br>room',
        webinar: 'Webinar room',
        short_links_br: 'Short<br>links',
        short_links: 'Short links',
        site_constr_br: 'Site<br>constructor',
        site_constr: 'Site constructor',
        shop_br: 'Internet<br>shop',
        shop: 'Internet shop',
        more: 'More',
        mail: 'POCHTA+',
        more_m: 'More'
    },
    currencies: {
        title: 'Exchange rates of the Central Bank of the Russian Federation for today',
    },
    footer: {
        adv: 'Advertisement',
        all_services: 'All services',
        help: 'Help',
        rules_1: 'Terms of use',
        rules_2: 'Privacy policy',
    },
    sidebar: {
        business: 'Online business',
        video: 'Video blog',
        market: 'Alladin market',
        app_shop: 'App shop',
        mail: 'Mail service',
        disk: 'Disk service',
        messenger: 'Messenger Connect',
        short_links: 'Short links service',
        webinar: 'Webinar room',
        site_constr: 'Site constructor',
        photo: 'Photogram',
        soc_net: 'Social network',
        star_map: 'Star map',
        adv: 'Place an advertisment',
        help: 'Support service',
        all_services: 'All services'
    },
    agreement: {
        main_page: '« Home',
        title: 'Terms of use',
        text: 'This User Agreement (hereinafter referred to as the Agreement) regulates the relationship between Sukhba JSC (hereinafter Search or Administration) on the one hand and the site user on the other.<br>\n' +
            '              The Search site is not a mass media.<br>\n' +
            '              <br>\n' +
            '              By using the site, you agree to the terms of this agreement.<br>\n' +
            '              If you do not agree with the terms of this agreement, do not use the Search site!<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Rights and obligations of the parties<br>\n' +
            '              The user has the right:<br>\n' +
            '              - search for information on the site<br>\n' +
            '              - receive information on the site<br>\n' +
            '              - copy information to other sites indicating the source<br>\n' +
            '              - copy information to other sites with the permission of the copyright holder<br>\n' +
            '              - require the administration to hide any information about the user<br>\n' +
            '              - use the site information for personal non-commercial purposes<br>\n' +
            '              <br>\n' +
            '              The administration has the right:<br>\n' +
            '              - at your discretion and the need to create, change, cancel rules<br>\n' +
            '              - restrict access to any information on the site<br>\n' +
            '              - create, modify, delete information<br>\n' +
            '              <br>\n' +
            '              The user undertakes:<br>\n' +
            '              - do not disrupt the site\'s performance<br>\n' +
            '              - do not use scripts (programs) for the automated collection of information and / or interaction with the Site and its Services<br>\n' +
            '              <br>\n' +
            '              Administration undertakes:<br>\n' +
            '              - maintain the site\'s performance, except in cases where it is impossible for reasons beyond the control of the Administration.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Liability of the parties<br>\n' +
            '              - the administration does not bear any responsibility for the accuracy of information copied from other sources<br>\n' +
            '              - the administration does not bear any responsibility for the services provided by third parties<br>\n' +
            '              - in the event of a force majeure situation (military action, state of emergency, natural disaster, etc.), the Administration does not guarantee the safety of the information posted by the User, as well as the uninterrupted operation of the information resource<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Terms of the Agreement<br>\n' +
            '              This Agreement enters into force with any use of this site.<br>\n' +
            '              The agreement expires when a new version of it appears.<br>\n' +
            '              The administration reserves the right to unilaterally change this agreement at its discretion.<br>\n' +
            '              The administration does not notify users about changes in the Agreement.<br>'
    },
    privacy_policy: {
        main_page: '« Home',
        title: 'Privacy Policy',
        text: 'The personal data processing policy (hereinafter referred to as the Privacy Policy or the Privacy Policy) was developed in accordance with the Federal Law of July 27, 2006. № 152-ФЗ "On personal data" (hereinafter - ФЗ-152).<br>\n' +
            '              <br>\n' +
            '              This Policy determines the procedure for processing personal data and measures to ensure the security of personal data at the owner of the site of JSC Sukhba (hereinafter referred to as Search or Operator).<br>\n' +
            '              <br>\n' +
            '              The purpose of this privacy policy is to ensure the protection of human and civil rights and freedoms when processing his personal data, including the protection of the rights to privacy, personal and family secrets.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              If you do not agree with the terms of our Privacy Policy, do not use the site www.sba.plus! <br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              This Policy contains the following information:<br>\n' +
            '              <br>\n' +
            '              - purpose of processing personal data; <br>\n' +
            '              - a list of personal data for the processing of which the consent of the subject of personal data is given; <br>\n' +
            '              - name or surname, first name, patronymic and address of the person who processes personal data on behalf of the operator, if the processing is entrusted to such a person; <br>\n' +
            '              - a list of actions with personal data, for the performance of which consent is given, a general description of the methods used by the operator for processing personal data; <br>\n' +
            '              - the period during which the consent of the subject of personal data is valid, as well as the method of its withdrawal, unless otherwise established by federal law;<br>\n' +
            '              - information on how you can withdraw your consent to the processing of personal data. <br>\n' +
            '              <br>\n' +
            '              Basic concepts used in this privacy policy:<br>\n' +
            '              <br>\n' +
            '              Personal data - any information relating directly or indirectly to a specific or identifiable individual (personal data subject); <br>\n' +
            '              Operator - a state body, a municipal body, a legal or natural person, independently or jointly with other persons organizing and (or) processing personal data, as well as determining the purposes of processing personal data, the composition of personal data to be processed, actions (operations) performed with personal data; <br>\n' +
            '              Processing of personal data - any action (operation) or a set of actions (operations) performed using automation tools or without using such tools with personal data, including collection, recording, systematization, accumulation, storage, clarification (update, change), extraction, use, transfer (distribution, provision, access), depersonalization, blocking, deletion, destruction of personal data;<br>\n' +
            '              <br>\n' +
            '              Principles of collecting personal data:<br>\n' +
            '              <br>\n' +
            '              - The processing of personal data is carried out on a legal and fair basis <br>\n' +
            '              - The processing of personal data is limited to the achievement of specific, predetermined and legal purposes. <br>\n' +
            '              - It is not allowed to combine databases containing personal data, the processing of which is carried out for purposes incompatible with each other. <br>\n' +
            '              - Only personal data that meet the purposes of their processing is subject to processing. <br>\n' +
            '              - The content and volume of processed personal data correspond to the stated purposes of processing. <br>\n' +
            '              - When processing personal data, the accuracy of personal data, their sufficiency, and, if necessary, relevance in relation to the purposes of processing personal data are ensured. <br>\n' +
            '              - The operator takes the necessary measures or ensures that they are taken to delete or clarify incomplete or inaccurate data. <br>\n' +
            '              - The storage of personal data is carried out in a form that allows you to determine the subject of personal data, no longer than the purpose of processing personal data requires, unless the storage period for personal data is established by federal law, an agreement to which the subject of personal data is a party, beneficiary or guarantor. The processed personal data are subject to destruction or depersonalization upon achievement of the processing goals or in case of loss of the need to achieve these goals, unless otherwise provided by federal law. <br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              List of collected personal data<br>\n' +
            '              The operator does not collect any personal information about the user. However, the user\'s personal information may be collected by external organizations (third parties) that provide tools to collect information about the state of the site. For this reason, the Operator disclaims responsibility for maintaining the confidentiality of user data. The privacy terms of third parties, the user can read on their websites.<br>\n' +
            '              <br>\n' +
            '              <br>\n' +
            '              Other information\n<br>\n' +
            '              <br>\n' +
            '              - Privacy Policy edition from 28-12-2018 <br>\n' +
            '              - The term of this Privacy Policy is unlimited <br>\n' +
            '              - This Privacy Policy becomes invalid when a new version appears on the website https://www.sba.plus/ <br>\n' +
            '              - Children of all ages can freely use this website.<br>'
    }
};
