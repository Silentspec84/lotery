(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[25],{

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/assets/sass/themes/layout/brand/light.scss":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./resources/js/src/assets/sass/themes/layout/brand/light.scss ***!
  \********************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "@media (min-width: 992px) {\n  .brand {\n    background-color: #ffffff;\n    box-shadow: none;\n  }\n  .brand .btn i {\n    color: #b4b8ce;\n  }\n  .brand .btn .svg-icon svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #b4b8ce;\n  }\n  .brand .btn .svg-icon svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .brand .btn:hover .svg-icon svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #3699FF;\n  }\n  .brand .btn:hover .svg-icon svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .brand .btn:hover i {\n    color: #3699FF;\n  }\n}\n@media (max-width: 991.98px) {\n  .header-mobile {\n    background-color: #ffffff;\n    box-shadow: 0px 1px 9px -3px rgba(0, 0, 0, 0.1);\n  }\n  .header-mobile .burger-icon span {\n    background-color: #b4b8ce;\n  }\n  .header-mobile .burger-icon span::before, .header-mobile .burger-icon span::after {\n    background-color: #b4b8ce;\n  }\n  .header-mobile .burger-icon:hover span {\n    background-color: #3699FF;\n  }\n  .header-mobile .burger-icon:hover span::before, .header-mobile .burger-icon:hover span::after {\n    background-color: #3699FF;\n  }\n  .header-mobile .burger-icon-active span {\n    background-color: #3699FF;\n  }\n  .header-mobile .burger-icon-active span::before, .header-mobile .burger-icon-active span::after {\n    background-color: #3699FF;\n  }\n  .header-mobile .btn i {\n    color: #b4b8ce;\n  }\n  .header-mobile .btn .svg-icon svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #b4b8ce;\n  }\n  .header-mobile .btn .svg-icon svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .header-mobile .btn:hover .svg-icon svg g [fill] {\n    transition: fill 0.3s ease;\n    fill: #3699FF;\n  }\n  .header-mobile .btn:hover .svg-icon svg:hover g [fill] {\n    transition: fill 0.3s ease;\n  }\n  .header-mobile .btn:hover i {\n    color: #3699FF;\n  }\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./resources/js/src/assets/sass/themes/layout/brand/light.scss":
/*!*********************************************************************!*\
  !*** ./resources/js/src/assets/sass/themes/layout/brand/light.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../../../node_modules/css-loader/dist/cjs.js!../../../../../../../../node_modules/postcss-loader/src??ref--7-2!../../../../../../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!./light.scss */ "./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./resources/js/src/assets/sass/themes/layout/brand/light.scss");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ })

}]);