<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Регистрация, вход в ЛК, восстановление пароля
 */
Route::group([
    'as' => 'auth.', // имя маршрута, например auth.index
    'prefix' => 'auth', // префикс маршрута, например auth/index
], function () {
    Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register')
        ->name('register');
    Route::post('login', 'App\Http\Controllers\Auth\LoginController@login')
        ->name('login');
    Route::get('logout', 'App\Http\Controllers\Auth\LoginController@logout')
        ->name('logout');
});

Route::post('/api/get-referals', 'App\Http\Controllers\SiteController@getReferals');
Route::post('/api/visitor', 'App\Http\Controllers\SiteController@visitor');
Route::post('/api/get-today-stats', 'App\Http\Controllers\SiteController@getTodayStats');
Route::post('/api/get-advs', 'App\Http\Controllers\SiteController@getAdvs');
Route::post('/api/get-ticket', 'App\Http\Controllers\LotteryController@getTicket');
Route::post('/api/get-yesturday-stats', 'App\Http\Controllers\SiteController@getYesturdayStats');
Route::post('/api/get-yesturday-win-tickets', 'App\Http\Controllers\LotteryController@getYesturdayWinTickets');
Route::post('/api/check-ticket', 'App\Http\Controllers\LotteryController@checkTicket');
Route::post('/api/get-win', 'App\Http\Controllers\LotteryController@getWin');
Route::post('/api/get-pay-data', 'App\Http\Controllers\PayController@getPayData');
Route::post('/api/withdraw', 'App\Http\Controllers\PayController@withwraw');

Route::get('/api/run-player', 'App\Http\Controllers\SiteController@runPlayer');
Route::post('/api/stop-player', 'App\Http\Controllers\SiteController@stopPlayer');
Route::post('/api/get-player', 'App\Http\Controllers\SiteController@getPlayer');
Route::get('/api/run-winner', 'App\Http\Controllers\SiteController@runWinner');
Route::post('/api/stop-winner', 'App\Http\Controllers\SiteController@stopWinner');
Route::post('/api/get-winner', 'App\Http\Controllers\SiteController@getWinner');

Route::post('/api/get-user', 'App\Http\Controllers\SiteController@getUser');

Route::post('/api/register', 'App\Http\Controllers\SiteController@register');

Route::get('/web', 'App\Http\Controllers\Search\WebController@index');
Route::post('/web/filters', 'App\Http\Controllers\Search\WebController@filters');

Route::get('/images/results', 'App\Http\Controllers\Search\ImagesController@index');
Route::post('/images/filters', 'App\Http\Controllers\Search\ImagesController@filters');

Route::get('/videos/results', 'App\Http\Controllers\Search\VideosController@index');
Route::post('/videos/filters', 'App\Http\Controllers\Search\VideosController@filters');

Route::get('/news/results', 'App\Http\Controllers\Search\NewsController@index');
Route::post('/news/filters', 'App\Http\Controllers\Search\NewsController@filters');

Route::post('/get-search-settings', 'App\Http\Controllers\Search\SettingsController@getSearchSettings');
Route::post('/save-search-settings', 'App\Http\Controllers\Search\SettingsController@saveSearchSettings');

Route::post('/suggestions', 'App\Http\Controllers\Search\SuggestionsController@index');
Route::post('/filters', 'App\Http\Controllers\Search\FiltersController@index');

Route::get('{any}', function () {
    return view('app');
})->where('any', '.*');
