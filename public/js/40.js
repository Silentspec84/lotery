(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[40],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _view_content_CodePreview_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/view/content/CodePreview.vue */ "./resources/js/src/view/content/CodePreview.vue");
/* harmony import */ var _core_services_store_breadcrumbs_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/core/services/store/breadcrumbs.module */ "./resources/js/src/core/services/store/breadcrumbs.module.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      html1: "<div>\n    <b-button>Button</b-button>\n    <b-button variant=\"danger\">Button</b-button>\n    <b-button variant=\"success\">Button</b-button>\n    <b-button variant=\"outline-primary\">Button</b-button>\n</div>",
      html2: "<div>\n    <b-button>I am a Button</b-button>\n    <b-button href=\"#\">I am a Link</b-button>\n</div>",
      html3: "<b-row>\n    <b-col lg=\"4\" class=\"pb-2\">\n        <b-button size=\"sm\">Small Button</b-button>\n    </b-col>\n    <b-col lg=\"4\" class=\"pb-2\">\n        <b-button>Default Button</b-button>\n    </b-col>\n    <b-col lg=\"4\" class=\"pb-2\">\n        <b-button size=\"lg\">Large Button</b-button>\n    </b-col>\n</b-row>",
      html4: "<div>\n    <b-button pill>Button</b-button>\n    <b-button pill variant=\"primary\">Button</b-button>\n    <b-button pill variant=\"outline-secondary\">Button</b-button>\n    <b-button pill variant=\"success\">Button</b-button>\n    <b-button pill variant=\"outline-danger\">Button</b-button>\n    <b-button pill variant=\"info\">Button</b-button>\n</div>",
      html5: "<div>\n    <b-button squared>Button</b-button>\n    <b-button squared variant=\"primary\">Button</b-button>\n    <b-button squared variant=\"outline-secondary\">Button</b-button>\n    <b-button squared variant=\"success\">Button</b-button>\n    <b-button squared variant=\"outline-danger\">Button</b-button>\n    <b-button squared variant=\"info\">Button</b-button>\n</div>",
      html6: "<div>\n    <h5>Pressed and un-pressed state</h5>\n    <b-button :pressed=\"true\" variant=\"success\">Always Pressed</b-button>\n    <b-button :pressed=\"false\" variant=\"success\">Not Pressed</b-button>\n\n    <h5 class=\"mt-3\">Toggleable Button</h5>\n    <b-button :pressed.sync=\"myToggle\" variant=\"primary\">Toggle Me</b-button>\n    <p>Pressed State: <strong>{{ myToggle }}</strong></p>\n\n    <h5>In a button group</h5>\n    <b-button-group size=\"sm\">\n        <b-button\n                v-for=\"(btn, idx) in buttons\"\n                :key=\"idx\"\n                :pressed.sync=\"btn.state\"\n                variant=\"primary\"\n        >\n            {{ btn.caption }}\n        </b-button>\n    </b-button-group>\n    <p>Pressed States: <strong>{{ btnStates }}</strong></p>\n</div>",
      js6: "export default {\n    data() {\n      return {\n        myToggle: false,\n        buttons: [\n          { caption: 'Toggle 1', state: true },\n          { caption: 'Toggle 2', state: false },\n          { caption: 'Toggle 3', state: true },\n          { caption: 'Toggle 4', state: false }\n        ]\n      }\n    },\n    computed: {\n      btnStates() {\n        return this.buttons.map(btn => btn.state)\n      }\n    }\n  }",
      myToggle: false,
      buttons: [{
        caption: "Toggle 1",
        state: true
      }, {
        caption: "Toggle 2",
        state: false
      }, {
        caption: "Toggle 3",
        state: true
      }, {
        caption: "Toggle 4",
        state: false
      }]
    };
  },
  components: {
    KTCodePreview: _view_content_CodePreview_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  mounted: function mounted() {
    this.$store.dispatch(_core_services_store_breadcrumbs_module__WEBPACK_IMPORTED_MODULE_1__["SET_BREADCRUMB"], [{
      title: "Vue Bootstrap",
      route: "alert"
    }, {
      title: "Buttons"
    }]);
  },
  computed: {
    btnStates: function btnStates() {
      return this.buttons.map(function (btn) {
        return btn.state;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "b-alert",
        {
          staticClass:
            "alert alert-custom alert-white alert-shadow fade show gutter-b",
          attrs: { show: "", variant: "light" }
        },
        [
          _c("div", { staticClass: "alert-icon" }, [
            _c(
              "span",
              { staticClass: "svg-icon svg-icon-lg" },
              [
                _c("inline-svg", {
                  attrs: { src: "media/svg/icons/Tools/Compass.svg" }
                })
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "alert-text" }, [
            _c("b", [_vm._v("Buttons")]),
            _vm._v(
              " Use Bootstrap's custom b-button component for actions in\n      forms, dialogs, and more. Includes support for a handful of contextual\n      variations, sizes, states, and more.\n      "
            ),
            _c(
              "a",
              {
                staticClass: "font-weight-bold",
                attrs: {
                  href: "https://bootstrap-vue.js.org/docs/components/button",
                  target: "_blank"
                }
              },
              [_vm._v("\n        See documentation.\n      ")]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-md-6" },
          [
            _c("KTCodePreview", {
              attrs: { title: "Buttons" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c(
                        "div",
                        [
                          _c("b-button", { staticClass: "mr-3" }, [
                            _vm._v("Button")
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { variant: "danger" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { variant: "success" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { variant: "outline-primary" }
                            },
                            [_vm._v("Button")]
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html1) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Element type" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c(
                        "div",
                        [
                          _c("b-button", { staticClass: "mr-3" }, [
                            _vm._v("I am a Button")
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            { staticClass: "mr-3", attrs: { href: "#" } },
                            [_vm._v("I am a Link")]
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html2) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Sizing" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Fancy larger or smaller buttons? Specify "
                        ),
                        _c("code", [_vm._v("lg")]),
                        _vm._v(" or\n            "),
                        _c("code", [_vm._v("sm")]),
                        _vm._v(" via the "),
                        _c("code", [_vm._v("size")]),
                        _vm._v(" prop.\n          ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "b-row",
                        [
                          _c(
                            "b-col",
                            { staticClass: "pb-2", attrs: { lg: "4" } },
                            [
                              _c("b-button", { attrs: { size: "sm" } }, [
                                _vm._v("Small Button")
                              ])
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "b-col",
                            { staticClass: "pb-2", attrs: { lg: "4" } },
                            [_c("b-button", [_vm._v("Default Button")])],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "b-col",
                            { staticClass: "pb-2", attrs: { lg: "4" } },
                            [
                              _c("b-button", { attrs: { size: "lg" } }, [
                                _vm._v("Large Button")
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html3) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            })
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-md-6" },
          [
            _c("KTCodePreview", {
              attrs: { title: "Pill style" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Prefer buttons with a more rounded-pill style? Just set the prop\n            "
                        ),
                        _c("code", [_vm._v("pill")]),
                        _vm._v(" to true.\n          ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c(
                            "b-button",
                            { staticClass: "mr-3", attrs: { pill: "" } },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pill: "", variant: "primary" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pill: "", variant: "outline-secondary" }
                            },
                            [_vm._v("\n              Button\n            ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pill: "", variant: "success" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pill: "", variant: "outline-danger" }
                            },
                            [_vm._v("\n              Button\n            ")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pill: "", variant: "info" }
                            },
                            [_vm._v("Button")]
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html4) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Squared style" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Prefer buttons with a more square corner style? Just set the prop\n            "
                        ),
                        _c("code", [_vm._v("squared")]),
                        _vm._v(" to true.\n          ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c(
                            "b-button",
                            { staticClass: "mr-3", attrs: { squared: "" } },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { squared: "", variant: "primary" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: {
                                squared: "",
                                variant: "outline-secondary"
                              }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { squared: "", variant: "success" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { squared: "", variant: "outline-danger" }
                            },
                            [_vm._v("Button")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { squared: "", variant: "info" }
                            },
                            [_vm._v("Button")]
                          )
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html5) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            }),
            _vm._v(" "),
            _c("KTCodePreview", {
              attrs: { title: "Pressed state and toggling" },
              scopedSlots: _vm._u([
                {
                  key: "preview",
                  fn: function() {
                    return [
                      _c("p", [
                        _vm._v(
                          "\n            Buttons will appear pressed (with a darker background, darker\n            border, and inset shadow) when the prop "
                        ),
                        _c("code", [_vm._v("pressed")]),
                        _vm._v(" is\n            set to true.\n          ")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        [
                          _c("h5", [_vm._v("Pressed and un-pressed state")]),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pressed: true, variant: "success" }
                            },
                            [_vm._v("Always Pressed")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: { pressed: false, variant: "success" }
                            },
                            [_vm._v("Not Pressed")]
                          ),
                          _vm._v(" "),
                          _c("h5", { staticClass: "mt-3" }, [
                            _vm._v("Toggleable Button")
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-button",
                            {
                              staticClass: "mr-3",
                              attrs: {
                                pressed: _vm.myToggle,
                                variant: "primary"
                              },
                              on: {
                                "update:pressed": function($event) {
                                  _vm.myToggle = $event
                                }
                              }
                            },
                            [_vm._v("Toggle Me")]
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "mt-3" }, [
                            _vm._v("\n              Pressed State: "),
                            _c("strong", [_vm._v(_vm._s(_vm.myToggle))])
                          ]),
                          _vm._v(" "),
                          _c("h5", [_vm._v("In a button group")]),
                          _vm._v(" "),
                          _c(
                            "b-button-group",
                            { attrs: { size: "sm" } },
                            _vm._l(_vm.buttons, function(btn, idx) {
                              return _c(
                                "b-button",
                                {
                                  key: idx,
                                  staticClass: "mr-3",
                                  attrs: {
                                    pressed: btn.state,
                                    variant: "primary"
                                  },
                                  on: {
                                    "update:pressed": function($event) {
                                      return _vm.$set(btn, "state", $event)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                " +
                                      _vm._s(btn.caption) +
                                      "\n              "
                                  )
                                ]
                              )
                            }),
                            1
                          ),
                          _vm._v(" "),
                          _c("p", { staticClass: "mt-3" }, [
                            _vm._v("\n              Pressed States: "),
                            _c("strong", [_vm._v(_vm._s(_vm.btnStates))])
                          ])
                        ],
                        1
                      )
                    ]
                  },
                  proxy: true
                },
                {
                  key: "html",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.html6) + "\n        ")
                    ]
                  },
                  proxy: true
                },
                {
                  key: "js",
                  fn: function() {
                    return [
                      _vm._v("\n          " + _vm._s(_vm.js6) + "\n        ")
                    ]
                  },
                  proxy: true
                }
              ])
            })
          ],
          1
        )
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue":
/*!********************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Button.vue?vue&type=template&id=67c6e9d4& */ "./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4&");
/* harmony import */ var _Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Button.vue?vue&type=script&lang=js& */ "./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/view/pages/admin/vue-bootstrap/Button.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Button.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Button.vue?vue&type=template&id=67c6e9d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/view/pages/admin/vue-bootstrap/Button.vue?vue&type=template&id=67c6e9d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_67c6e9d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);