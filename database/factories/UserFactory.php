<?php

namespace Database\Factories;

use App\Models\User;
use Carbon\Carbon;
use Dirape\Token\Token;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'email' => $this->faker->unique()->safeEmail,
            'email_verified_at' => Carbon::now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role' => 'user',
            'balance' => 0,
            'lotteries' => 5,
            'ref_link' => (new Token)->RandomString(12),
            'referer_id' => null,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'last_login' => Carbon::now(),
        ];
    }
}
